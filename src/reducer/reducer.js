import {combineReducers} from 'redux';
import rootReducer from './rootReducer';

export default reducer = combineReducers({
    rootReducer:rootReducer
});