const defaultState = {
	Account: {},
	count:0,
};
export default (state = defaultState, action) => {
	switch (action.type) {
		case 'INC_COUNT': return {
			...state,
			count: state.count+1,
		};
		case 'DEC_COUNT': return {
			...state,
			count: state.count-1,
		};
		default:
			break;
	}
	return state;
};