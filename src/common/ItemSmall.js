import React, { Component } from 'react'
import {
    Text, View, StyleSheet, Image, TouchableOpacity
} from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons';

//import
import STYLES from '../config/styles.config';
import colors from '../config/colors';
import { HEIGHT, WIDTH } from '../config/Function';

type Props = {
    title: string,
    onButton: Function,
}
export default ItemSmall = (props: any) => {
    const { icon, title, describe, onButton } = props;
    return (
        <TouchableOpacity style={styles.viewItem} onPress={() => onButton()}>
            <View style={{flex:0, flexDirection:'row'}}>
                {
                    icon
                }
                <View style={styles.viewTextLeft}>
                    <Text style={styles.textNameFolder}>{title}</Text>
                    <Text numberOfLines={3} style={styles.textNote}>{describe}</Text>
                </View>
            </View>

            <Ionicons size={WIDTH(20)} name={'ios-arrow-forward'} />
        </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    viewItem: {
        flexDirection: 'row',
        width: WIDTH(360),
        paddingHorizontal: WIDTH(10),
        paddingVertical: HEIGHT(12),
        justifyContent: 'space-between',
        marginBottom: HEIGHT(5),
        backgroundColor: 'white',
        alignItems: 'center',
        // borderTopColor: '#ddd', borderTopWidth: 1
    },
    viewTextLeft: {
        marginLeft: STYLES.widthScreen * 10 / 360,
        width: STYLES.widthScreen * 0.65
    },
    textNameFolder: {fontWeight:'400', fontSize: STYLES.fontSizeText, color: colors.colorBlack },
    textNote: { fontSize: STYLES.fontSizeNormalText, marginTop: 5 / 640 * STYLES.heightScreen },
    text: {
        color: colors.colorWhite,
        fontSize: STYLES.fontSizeText,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
    },
})

