import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,
} from 'react-native'

//import
import IMAGE from '../assets/image';
import STYLES from '../config/styles.config';
import colors from '../config/colors';

type Props = {
  title:string,
  onButton:Function,
}
export default ChooseDate = (props: any) => {
    const {title,onButton}= props;  
    return (
     <View style={styles.container}>
            <Text style={styles.text}>{title}</Text>
      </View>
    )
}
const styles = StyleSheet.create({
      container: {
        flex:1,
      },
      text: {
        color: Colors.colorWhite,
        fontSize: STYLES.fontSizeText,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
      },
})

