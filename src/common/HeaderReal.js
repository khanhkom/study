import React, { Component } from 'react'
import {
  Text, View, StyleSheet, Image,TextInput,TouchableOpacity,StatusBar
} from 'react-native'

//import from Lib
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';

//import
import STYLES from '../config/styles.config';
import Colors from '../config/colors';
import { WIDTH } from '../config/Function';

type Props = {
  title:string,
  onButton:Function,
  onButtonRight:Function,
  linkImage:any,
  colorImage:any,
  save:any,
}
export default HeaderReal = (props: any) => {
    const {title,onButton,linkImage,onButtonRight,colorImage,save}= props;  
    return (
     <View style={styles.header}>
      <StatusBar backgroundColor={Colors.colorPink} />
      <TouchableOpacity
        onPress={()=>onButton()}
        style={styles.menu}
      >
          <Ionicons name="ios-arrow-back" size={26 * STYLES.heightScreen / 640} color="#fff" />
      </TouchableOpacity>      
        <View style={styles.cntTextInput}>
                <Text numberOfLines={1} style={[styles.text,{width:WIDTH(200),flexWrap:'wrap',fontSize:STYLES.fontSizeLabel,fontWeight:'bold'}]}>
                    {title}
                </Text>     
            </View> 
         {
          (linkImage!=undefined) && <TouchableOpacity
            style={{
              flex:0,
              width:55 * STYLES.heightScreen / 640,
              alignItems:'flex-end',
              marginRight:3*STYLES.widthScreen/360
              }}
            disabled={colorImage!='white' ? false : true}
            onPress={()=>onButtonRight()}
         >     
              <Feather 
              name={linkImage} 
              size={23 * STYLES.heightScreen / 640} 
              color={colorImage != undefined ? colorImage : 'white'} 
              />
         </TouchableOpacity>
         }  
         {
          (linkImage==undefined && save==undefined) && 
          <View style={{width:55 * STYLES.heightScreen / 640}}>

           </View>
         }    
         {
          (linkImage==undefined && save!=undefined) && 
          <TouchableOpacity 
               onPress={()=>onButtonRight()}
                style={{width:55 * STYLES.heightScreen / 640}}>
              <Text style={[styles.text,{
                fontSize:STYLES.fontSizeText,
                fontWeight:'bold'}]}>
                    {save}
                </Text>    
           </TouchableOpacity>
         }    
      </View>
    )
}
const styles = StyleSheet.create({
    header: {
        height: (43 * STYLES.heightScreen / 640),
        width: STYLES.widthScreen,
        flexDirection: 'row',
        backgroundColor: Colors.colorPink,
        // borderColor: Colors.red,
        // borderWidth: 0.5,
        elevation: 4,
        // shadowColor: Colors.red,
        // shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 0.8,
        // shadowRadius: 2,
        // elevation: 3,
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingRight: 5,
       
      },
      menu:{
        flex: 0,
        paddingLeft: 8,
        width:55 * STYLES.heightScreen / 640,
      },
      logo: {
        width: 26 * STYLES.heightScreen / 640,
        height: STYLES.heightScreen * (35 / 640),
      },
      cntlogoStart:{
        width: STYLES.widthScreen * (35 / 360),
        height: STYLES.heightScreen * (35 / 640),
      },
      logoStart: {
        width: STYLES.widthScreen * (35 / 360),
        height: STYLES.heightScreen * (25 / 640),
      },
      text: {
        color: Colors.colorWhite,
        fontSize: STYLES.fontSizeText,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily:'Roboto-Medium'
      },
      textMini:{
        color: Colors.colorWhite,
        fontSize: STYLES.fontSizeMiniText,
        textAlign: "center",
        textAlignVertical: "center",
        fontFamily: 'Roboto-Regular',
      },
      cntTextInput:{
        justifyContent:'center',
        alignItems: 'center',
        flex:0,
        //height: (30 * STYLES.heightScreen / 640),
    },
      textInput:{
        width: STYLES.widthScreen * 0.53,
        height: (30 * STYLES.heightScreen / 640),
        backgroundColor: Colors.header,
        fontSize: STYLES.fontSizeNormalText,
        fontFamily: 'Roboto-Regular',
        marginLeft: 5,
        //textAlign:'center',
        paddingTop: 0, paddingBottom: 0,
        textAlignVertical:'center',
        color: Colors.white,
      }
})
