//import lib
import {  createStackNavigator,createAppContainer } from 'react-navigation';
import React from 'react';

import Details from '../container/Details/Details';
import Home from '../container/Home/Home';
import MapScreen from '../container/Map/MapScreen';
import SearchMain from '../container/Map/Search/SearchMain';
import GPlacesDemo from '../container/Map/GoogleSearch/GoogleSearch';
// import App from '../container/Study/Tinder/components/index';
// import App from '../container/Study/InstagramStory/App';
import App from '../container/Study/SpotifyHeader/components/App';
import ThongKe from '../container/Study/ThongKe/ThongKe';
import ReportLv1 from '../container/Study/ThongKe/ReportLv1';
import ReportLv2 from '../container/Study/ThongKe/ReportLv2';
import JuiceCard from '../container/Study/JuiceCard/JuiceCard';
import Travel from '../container/Study/TravelApp.js/Travel';
import XemDiem from '../container/Study/ThongTinLienQuan/XemDiem';
import Art from '../container/Study/Art/Art'
console.disableYellowBox = true
const AppNavigator = createStackNavigator({
	Details: { screen: Details },
	Home: { screen: Home },
	MapScreen:{screen:MapScreen},
	SearchMain :{screen:SearchMain},
	GPlacesDemo:{screen:GPlacesDemo},
	App:{screen:App},
	ThongKe:{screen:ThongKe},
	ReportLv1:{screen:ReportLv1},
	ReportLv2:{screen:ReportLv2},
	JuiceCard:{screen:JuiceCard},
	Travel:{screen:Travel},
	XemDiem:{screen:XemDiem},
	Art:{screen:Art}
}, {
		initialRouteName: 'Art',
		swipeEnabled: true,
		animationEnabled: false,
		headerMode: 'none',
		navigationOptions: {
			header: null
		},
		lazy: true,
		cardStyle: {
			backgroundColor: '#FFF',
			opacity: 1
		},
	})
const Router=createAppContainer(AppNavigator);
export default Router;
