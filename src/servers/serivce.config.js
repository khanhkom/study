

export const ROOT = 'http://172.104.41.68:3030';
export const SOCKET = 'http://172.104.41.68:9090';
export const GetEventsByTime='http://123.25.30.61:999/Web2App.asmx/GetEventsByTime';
export const GetNewsByTime='http://123.25.30.61:999/Web2App.asmx/GetNewsByTime';
export const GetEventBySearch='http://123.25.30.61:999/Web2App.asmx/GetEventBySearch';
export const GetNewsBySearch='http://123.25.30.61:999/Web2App.asmx/GetNewsBySearch';
export const SetEventsByTime='http://123.25.30.61:999/Web2App.asmx/SetEventsRead';
export const GetStaffByKey='http://123.25.30.61:999/Web2App.asmx/GetStaffByKey';
export const CreateEvent='http://123.25.30.61:999/Web2App.asmx/CreateEvent';
export const UploadFilesEvent='http://123.25.30.61:999/Web2App.asmx/UploadFilesEvent';

export const LOGIN_SERVICE = 'http://123.25.30.61:999/Web2App.asmx/Login';
export async function FetchJsonGet(url) {
	const myRequest = new Request(url, {
		method: "GET",
		headers: {
			"Content-Type": "application/json"
		},
	});
	return await fetch(myRequest)
		.then(response => {
			response.json()})
		.then(responseJson => {
			return responseJson;
		})
		.catch(error => {
			console.debug(error);
		});
}
export async function FetchDeleteWithTokenGet(url, accessToken, json) {
	const myRequest = new Request(url, {
		method: "DELETE",
		headers: {
			Authorization: "Bearer " + accessToken,
			"Content-Type": "application/json"
		},
		body: JSON.stringify(json)
	});
	return await fetch(myRequest)
		.then(response => response.json())
		.then(responseJson => {
			return responseJson;
		})
		.catch(error => {
			console.debug(error);
		});
}

export async function FetchJsonWithTokenGet(url, accessToken) {
	const myRequest = new Request(url, {
		method: "GET",
		headers: {
			Authorization: "Bearer " + accessToken,
		}
	});
	return await fetch(myRequest)
		.then(response => response.json())
		.then(responseJson => {
			return responseJson;
		})
		.catch(error => {
			console.debug(error);
		});
}

export async function FetchJsonWithToken(url, accessToken, json) {
	const myRequest = new Request(url, {
		method: "POST",
		headers: {
			Authorization: "Bearer " + accessToken,
			Accept: "application/json",
			"Content-Type": "application/json"
		},
		body: JSON.stringify(json)
	});
	return await fetch(myRequest)
		.then(response => {
			if (response.status === 200) {
				return response.json();
			} else {
				console.debug("Something went wrong on api server!");
			}
		})
		.then(response => {
			return response;
		})
		.catch(error => {
			console.debug(error);
		});
}


export async function FetchJson(url, json) {
	const myRequest = new Request(url, {
		method: "POST",
		headers: {
			Accept: "application/json",
			"Content-Type": "application/json"
		},
		body: JSON.stringify(json)
	});
	return await fetch(myRequest)
		.then(response => {
			if (response.status === 200) {
				return response.json();
			} else {
				console.debug("Something went wrong on api server!");
			}
		})
		.then(response => {
			return response;
		})
		.catch(error => {
			console.debug(error);
		});
}
class Data {
	key_store;
	data;
	constructor(key_store, data) {
		this.key_store = key_store;
		this.data = data;
	}
}
export function FetchJsonWait(list, json) {
	return new Promise((resolve, reject) => {
		let fetches = [];
		let arr = [];
		list.map(v => {
			const myRequest = new Request(v.link, {
				method: "POST",
				headers: {
					Accept: "application/json",
					"Content-Type": "application/json"
				},
				body: JSON.stringify(json)
			});
			var key_store = v.key_store;
			fetches.push(
				fetch(myRequest)
					.then((data) => data.json())
					.then((dataJson) => {
						arr.push(new Data(key_store, dataJson.rows));
					})
			)
		});
		Promise.all(fetches).then(() => {
			resolve(arr);
		})
	});
}