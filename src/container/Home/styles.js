import {
    StyleSheet,
 } from 'react-native'
 
 //import
 import IMAGE from '../../assets/image';
 import STYLES from '../../config/styles.config';
 import colors from '../../config/colors';
 
 export default styles = StyleSheet.create({
     container: {
       flex:1,
     },
     cntMain:{
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
     },
     img:{
         width:STYLES.widthScreen*0.65,
         height:STYLES.heightScreen*0.25,
     },
     text:{
         fontSize:STYLES.fontSizeText,
         color:colors.colorPink,
     }
 })
 
 