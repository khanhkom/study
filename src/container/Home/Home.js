/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'
import {
    View, StyleSheet, Image, Text, TouchableOpacity
} from 'react-native'

import { connect } from 'react-redux';
//import config
import IMAGE from '../../assets/image';
import styles from './styles'

//import common

import HeaderReal from '../../common/HeaderReal';
import { TRANG_CHU } from '../../config/default';
import {incCount,decCount} from '../../actions/actionCreators'

class Home extends Component<Props> {
    render() {
        return (
            <View style={styles.container}>
                <HeaderReal
                    title={TRANG_CHU}
                    onButtonLeft={() => {}}
                    onButtonLeft={() => {}}
                    iconLeft={''}
                    iconRight={''}
                />
                <View style={styles.cntMain}>
                <Image
                    source={IMAGE.hoaphuong}
                    style={styles.img}
                />
                <TouchableOpacity
                    onPress={()=>this.props.navigation.navigate("Details")}
                >
                    <Text style={styles.text}>Chuyen sang man hinh Details</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={()=>this.props.incCount()}
                >
                    <Text style={styles.text}>Tang cout</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={()=>this.props.decCount()}
                >
                    <Text style={styles.text}>Giam count</Text>
                </TouchableOpacity>
                <View>
                    <Text>{this.props.count}</Text>
                </View>
                </View>
            </View>
        )
    }
}
function mapStateToProps(state) {
    return {
        count: state.rootReducer.count,
    };
}

export default connect(mapStateToProps, {
    incCount,decCount
})(Home);