/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'
import {
    View, StyleSheet, Image, Text, ScrollView
} from 'react-native'

import { connect } from 'react-redux';
//import config
import IMAGE from '../../assets/image';
import styles from './styles'

//import common

import HeaderReal from '../../common/HeaderReal';
import { CHI_TIET } from '../../config/default';

class Details extends Component<Props> {
    render() {
        return (
            <View style={styles.container}>
                <HeaderReal
                    title={CHI_TIET}
                    onButtonLeft={() => this.props.navigation.goBack()}
                    onButtonRight={() => {}}
                    iconLeft={'ios-arrow-back'}
                    iconRight={''}
                />
                <View style={{flex:1}}>
                    <Text>Man hinh chi tiet</Text>
                </View>
            </View>
        )
    }
}
function mapStateToProps(state) {
    return {
        count: state.rootReducer.count,
    };
}

export default connect(mapStateToProps, {
})(Details);