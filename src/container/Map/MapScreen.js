import React, { Component } from 'react';
import {
    Text,
    Image, Alert,
    StyleSheet, TouchableOpacity,
    Dimensions,
    View,

} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome'
import MapView from 'react-native-maps';
const { height, width } = Dimensions.get('window');
import styles from './styles';
var DATA = [
    {
        id: 0,
        name: "Cầu Long Biên",
        address: "2VV5+FV Bồ Đề, Long Biên, Hà Nội, Việt Nam",
        marker: {
            latitude: 21.0437409,
            longitude: 105.8575513
        },
        description: "Cầu Long Biên là cây cầu thép đầu tiên bắc qua sông Hồng, nối hai quận Hoàn Kiếm và Long Biên (Hà Nội), do Pháp xây dựng (1898-1920).Cầu dài 2.290 m vốn quen thuộc trong mắt người dân và khách du lịch từ góc nhìn ngang, hay góc hất từ dưới sông Hồng.Vì thế, có được một góc nhìn cầu Long Biên từ trên cao luôn là niềm mơ ước của nhiều người, đặc biệt những người yêu thích nhiếp ảnh.",
        image: [
            'https://we25.vn/media/Images/0(3)/12(2).jpg',
            'https://we25.vn/media/Images/00/11(2).jpg',
            'https://we25.vn/media/Images/0(1)/10(5).jpg',
            'https://we25.vn/media/Images/)0/Serein_2.jpg',
            'https://we25.vn/media/Images/00/Serein.jpg',
            'https://we25.vn/media/Images/00000/9.jpg',
            'https://we25.vn/media/Images/00000/8.jpg',
            'https://we25.vn/media/Images/0(2/7(3).jpg',
            'https://we25.vn/media/Images/0(2/2(11).jpg',
        ],
        type: "6",
        phonenumber: "",
    },
    {
        id: 1,
        name: "Bãi đá sông Hồng",
        address: "Ngõ 264 Âu Cơ, Nhật Tân, Tây Hồ, Hà Nội 100000, Việt Nam",
        marker: {
            latitude: 21.078527,
            longitude: 105.8330781
        },
        description: "Nếu có dịp ghé thăm bãi đá sông Hồng, bạn sẽ được tận hưởng một không gian thật thanh bình với trời trong, gió nhẹ cùng những hàng lau trắng bạt ngàn... Nơi đây sẽ là địa điểm không thể bỏ qua nếu bạn có ý định chụp ảnh để lưu giữ những khoảnh khắc tuyệt đẹp.",
        image: [
            'http://saigontv.news/wp-content/uploads/2018/05/chup-anh-50k3.jpg',
            'http://saigontv.news/wp-content/uploads/2018/05/chup-anh-50k4.jpg',
            'http://saigontv.news/wp-content/uploads/2018/05/chup-anh-50k6.jpg',
            'http://saigontv.news/wp-content/uploads/2018/05/chup-anh-50k5.jpg',
            'https://photo-1-baomoi.zadn.vn/w1000_r1/16/11/27/229/20936316/1_170310.jpg',
            'https://photo-1-baomoi.zadn.vn/w1000_r1/16/11/27/229/20936316/11_165665.jpg',
            'https://photo-1-baomoi.zadn.vn/w1000_r1/16/11/27/229/20936316/2_152514.jpg',
            'http://bit.ly/2ZbuHpI;http://media.we25.vn/images/(((0/21827373_1997418677181565_1294420821376237568_n-1024x768.jpg',
        ],
        phonenumber: "+84 24 6282 6888",
        type: "6"
    },
    {
        id: 2,
        name: "Nhà thờ Lớn",
        address: "40 Nhà Chung, Hàng Trống, Hoàn Kiếm, Hà Nội, Việt Nam",
        marker: {
            latitude: 21.028733,
            longitude: 105.846754
        },
        description: "Nhà thờ Lớn Hà Nội hay còn gọi là Nhà thờ Chính tòa Hà Nội là một công trình kiến trúc đặc sắc gắn liền với người dân Thủ đô qua nhiều thế kỷ. Nhà thờ là nơi sinh hoạt tín ngưỡng của các tín đồ theo đạo Thiên Chúa và là một địa điểm tham quan thu hút đông đảo du khách ở Hà Nội.",
        image: [
            'http://www.vietfuntravel.com.vn/image/data/Ha-Noi/nha-tho-lon/Gio-le-nha-tho-Lon-Ha-Noi-5.png',
            'http://static2.yan.vn/YanNews/2167221/201804/5-dia-diem-chup-anh-sieu-dep-ngay-tai-ha-noi-44be85e0.jpg',
            'http://a9.vietbao.vn/images/vn999/190/2015/05/20150527-facebook24h-angela-phuong-trinh-tung-tang-o-h-224-noi-1.jpg',
            'http://media.depplus.vn/images/site-1/20151120/web/nha-tho-nao-duoc-gioi-tre-pose-hinh-nhieu-nhat-241-233407.jpg',
            'https://innotour.vn/wp-content/uploads/2017/11/5-nha-tho-dep-nhat-viet-nam-1.jpg',
            'https://vngo.vn/upload/users/user1/image-1547700929.4291.jpg',
            'https://we25.vn/media/images/36-pho-phuong-29.jpg'
        ],
        phonenumber: "+84 24 6282 6888",
        type: "6"
    },

    {
        id: 3,
        name: "Xóm đường tàu",
        address: "phố Khâm Thiêm",
        marker: {
            latitude: 21.0190975,
            longitude: 105.8341665
        },
        description: "Xóm đường tàu đẹp như y như phố cổ Thập Phần ở Đài Bắc, Đài Loan.Và trở thành điểm 'check-in' mới của giới trẻ và các du khách nước ngoài.",
        image: [
            'https://photo-3-baomoi.zadn.vn/w1000_r1/2017_12_21_180_24364664/06932dd81399fac7a388.jpg',
            'https://media.metrip.vn/tour/content/thecrystyle.jpg',
            'https://static2.yan.vn/MYanNews/201810/kham-pha-xom-duong-tau-tai-ha-noi-2f0f0a21.jpg',
            'https://images.foody.vn/images/blogs/foody-upload-api-foody-whoaaaaaaaaa-636625940623467616-180522135242.jpg',
            'https://media.metrip.vn/tour/content/dianalidia.jpg;http://bizweb.dktcdn.net/thumb/grande/100/101/075/articles/xom.jpg?v=1539655243057',
            'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/48368646_2252243508325217_1864520224603111424_n.jpg?_nc_cat=105&_nc_oc=AQm0PhCx9wpNeRphVVDxOPAXFFuK5xRf0DKPWHM7pR50s4dQGGn2-sAQ7-s7jSpYumQ&_nc_ht=scontent.fhan2-4.fna&oh=75e68c1782eddf1bcf34989f95035a54&oe=5D73E6C8',
        ],
        phonenumber: "+84 24 6282 6888",
        type: "6"
    },
    {
        id: 4,
        name: "Hồ Hoàn Kiếm",
        address: "Hàng Trống, Hoàn Kiếm, Hà Nội, Việt Nam",
        marker: {
            latitude: 21.0287797,
            longitude: 105.850176
        },
        description: "Dù một mình hay với ai, hãy đến và cảm nhận nét đặc trưng của Hà Nội. Nghệ thuật đường phố, trò chơi dân gian,những bản tình ca ngẫu hứng, tìm lại chính mình, tìm lại chút ân tình xưa cũ, trải lòng bên mặt hồ.Bên cạnh đó, khám phá các khu phố cổ cũng vô cùng độc đáo, chắc chắn sẽ là địa điểm được check-in nhiều nhất ",
        image: [
            'https://blog.traveloka.com/source/uploads/sites/9/2018/11/dia-diem-song-ao-ha-noi-24.jpg',
            'https://scontent.fhan2-1.fna.fbcdn.net/v/t1.0-9/55790030_1246737122142120_7910726071513776128_n.jpg?_nc_cat=103&_nc_oc=AQkVc4mLwWsloQH2gYN2LI94ZWmQx7C6Vr9xZ_ZznnzGPAn8lzGR39tJsIZFqaxh9_Q&_nc_ht=scontent.fhan2-1.fna&oh=30a1c2a3358f5f16c0234940e5bb1520&oe=5D422143',
            'https://scontent.fhan2-4.fna.fbcdn.net/v/t1.0-9/56157759_1246737142142118_5927727301444763648_n.jpg?_nc_cat=104&_nc_oc=AQnIJaeY75zR9MyTBMjug3p04rtPglO6UDCFYD652HeHKWqfWRA7X1FQolHgsOQdcU4&_nc_ht=scontent.fhan2-4.fna&oh=96cd87c50b581049c2a86aa5fece1d57&oe=5D3947AC',
            'http://streaming1.danviet.vn/upload/1-2017/images/2017-01-16/14845623287022-anh-7.jpg',
        ],
        phonenumber: "+84 24 6282 6888",
        type: "6"
    },
    {
        id: 5,
        name: "Aon Mall Long Biên",
        address: "Số, 27 đường Cổ Linh, Long Biên, Hà Nội, Việt Nam",
        marker: {
            latitude: 21.0269951,
            longitude: 105.8968216
        },
        description: "Không hẳn là chốn dành riêng cho giới trẻ, nhưng sẽ cực kỳ tiếc nuối nếu như bạn bỏ qua khu trung tâm thương mại Aon Mall mới mở với tổ hợp ăn - chơi - mua sắm hiện đại và rất hay ho này.Cộng thêm hơi thở đặc trưng của người Nhật đã biến nơi đây thành một địa điểm nhất định phải check-in.",
        image: [
            'https://www.chudu24.com/wp-content/uploads/2018/05/n_huy_29738396_1642623139187129_5433719626187931648_n.jpg?fbclid=IwAR0JBY4Ee0u6eBPt_Z_a9JwgW0t5aGR9FlKNptTjFkXuSovV8QGlxGjsMEo',
            'https://kenh14cdn.com/2015/12247034-1072069992812474-7710240765990871577-n-1450763194986.jpg?fbclid=IwAR0f3SugVahYEbj9yf9jaih6xOfNkgRyGKNzAhnpMGIBu9Ir-Ilg1CBhjrQ',
            'https://i-ione.vnecdn.net/2015/11/28/trung-tam-thuong-mai-aeon-mall-5486-5793-1448684897.jpg?fbclid=IwAR0M7POPwR1_ZxhOmmFrbMmfOs21HiVE0OdAi2Elp6RgAxbsb8RR-4uJOns',
            'http://cdn01.diadiemanuong.com/ddau/640x/bai-gui-xe-o-aeon-mall-binh-tan-gay-sot-voi-diem-chup-hinh-cuc-chat-74a85901636046277465630559.jpg?fbclid=IwAR2zRFHZ0rXPI1sWJyPGZ21lFEPREpebO5JjYLaXiyciL51ie-w4-798hAQ',
            'https://media.metrip.vn/tour/content/dggmui_29november_32377363_210036679604478_2294008188174860288_n.jpg?fbclid=IwAR1W3VqFgPM8YHbQtJkyfNUzjA-GIe0fvCIcB8dUxO-OT3f484TElbu1gEY',
        ],
        phonenumber: "+84 24 6282 6888",
        type: "6"
    },

]
export default class ShowMap extends Component {

    constructor(props) {
        super(props)
        showDetail = false,
            arrayIdnear = [],
            this.state = {
                lat: null,
                long: null,
                places: null,
                idnear: [],
                region: {
                    latitude: 20.9822573,
                    longitude: 105.7888341,
                    latitudeDelta: 0.01,
                    longitudeDelta: 0.01,
                },
                location: {
                    latitude: 20.9822573,
                    longitude: 105.7888341,
                }
            }
    }
    componentWillMount() {
        // navigator.geolocation.getCurrentPosition(
        //     (position) => {
        //         const lat = position.coords.latitude;
        //         const long = position.coords.longitude;
        //         //   const location = JSON.stringify(position);     
        //         for (marker of DATA) {
        //             if (geolib.getDistance(position.coords, {
        //                 latitude: marker.marker.latitude,
        //                 longitude: marker.marker.longitude
        //             }) < 1000) {
        //                 arrayIdnear.push({
        //                     id: marker.id
        //                 })
        //             }
        //         }
        //         this.setState({
        //             lat:lat,
        //             long:long,
        //             region: {
        //                 latitude: position.coords.latitude,
        //                 longitude: position.coords.longitude,
        //                 latitudeDelta: 0.01,
        //                 longitudeDelta: 0.01,
        //             },
        //             location: {
        //                 latitude: position.coords.latitude,
        //                 longitude: position.coords.longitude
        //             },
        //             idnear: arrayIdnear
        //         });
        //     },
        //     error => Alert.alert(error.message),
        //     { enableHighAccuracy: false, timeout: 20000}
        // );
        // this.getPlace()
    }
    // getPlace() {
    //     const url = this.getUrlWithParameters(this.state.lat, this.state.long, 500, 'food', 'AIzaSyBD0D-lFl4rGzKdpQ9dJ-x279p3AJqB8To');
    //     fetch(url).then((data) => data.json())
    //         .then((res) => {
    //             console.log('zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz')
    //             console.log(res)
    //         }).catch((error) => {
    //             console.log('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
    //             console.error(error);
    //         })
    // }
    // getUrlWithParameters(lat, long, radius, type, API) {
    //     const url = 'https://googleapi/maps/api/place/nearbysearch/json?';
    //     const location = `location=${lat},${long}&radius=${radius}`;
    //     const typeData = `&type=${type}`;
    //     const key = `&key=${API}`;
    //     return `${url}${location}${typeData}${key}`;
    // }
    GetRange() {
        Alert.alert('Get range')
    }
    onRegionChange(data) {

    }
    showDetail(Places) {
        this.setState({
            Place: Places,
            showDetail: true
        })
    }
    onPress(data) {
        this.setState({
            showDetail: false,
        })
        //console.log(this.state.)
    }
    renderMarker() {
        markers = [];
        for (marker of this.state.markers) {
            markers.push(
                <MapView.Marker key={marker.latitude} coordinate={marker} title={'Local' + marker.longitude} description={'Detail'} />
            )
        }
        return markers;
    }
    render() {
        return (
            <View style={styles.container}>
                <MapView style={styles.map}
                    initialRegion={this.state.region}
                    onRegionChange={this.onRegionChange.bind(this)}
                    onPress={this.onPress.bind(this)}
                >
                    <MapView.Marker
                        key={this.state.location.latitude} coordinate={this.state.location} title={'My Location'} description={''} >
                        <View style={styles.radius}>
                            <View style={styles.marker}>
                            </View>
                        </View>
                    </MapView.Marker>
                    {this.state.idnear.map((marker) => {
                        return (
                            <MapView.Marker key={DATA[marker.id].marker.latitude} coordinate={DATA[marker.id].marker}
                                onPress={(e) => { e.stopPropagation(); this.showDetail(DATA[marker.id]) }}
                            >
                            </MapView.Marker>)
                    })}

                </MapView>
                {this.state.showDetail ? <View><Text>Chi tiet</Text></View> : []}
                <View style={styles.searchBox}>
                    <View style={styles.inputWrapper}>
                        <Text>tim kiem</Text>
                    </View>
                </View>
            </View>
        )
    }
}