import React, { Component } from 'react';


import _ from 'lodash'
import {
  View,
  Text,
  FlatList,
  ActivityIndicator,
  SafeAreaView
} from 'react-native';
import { ListItem, SearchBar } from 'react-native-elements';
import { getUsers,contains } from "./api/index";

  export default class SearchMain extends Component {
    constructor(props){
      super(props);
      this.state={
        loading: false,
        data: [],
        error: null,
        query:"",
        fullData:[]
      }
    }
    componentDidMount() {
      this.makeRemoteRequest();
    }
  
    makeRemoteRequest=_.debounce(()=> {
      this.setState({ loading: true });
  
      getUsers(20,this.state.query)
        .then(users => {
          this.setState({
            loading: false,
            data: users,
            fullData:users
          });
          console.log(this.state.query+'-----'+this.state.data)
        })
        .catch(error => {
          this.setState({ error, loading: false });
        })},250
    );
    ;
  
    renderSeparator=()=> {
      return (
        <View
          style={{
            height: 1,
            width: "86%",
            backgroundColor: "#CED0CE",
            marginLeft: "14%"
          }}
        />
      );
    };
  
    renderHeader =()=> {
      return <SearchBar placeholder="Type Here..." lightTheme round onChangeText={this.handleSearch} value={this.state.query}/>;
    };
  handleSearch = text=>{
    const formatQuery = text.toLowerCase();
    const data = _.filter(this.state.fullData,user=>{
     return contains(user,formatQuery)
    })
    this.setState({
      query:formatQuery,
      data,
    })
    this.makeRemoteRequest()
  }
    renderFooter=()=> {
      if (!this.state.loading) return null;
  
      return (
        <View
          style={{
            paddingVertical: 20,
            borderTopWidth: 1,
            borderColor: "#CED0CE"
          }}
        >
          <ActivityIndicator animating size="large" />
        </View>
      );
    };
    render(){
      return(
        <SafeAreaView>
            <FlatList
              data={this.state.data}
              renderItem={({ item }) => (
                <ListItem
                  roundAvatar
                  title={`${item.name.first} ${item.name.last}`}
                  subtitle={item.email}
                  leftAvatar={{rounded: true, source: { uri:item.picture.thumbnail } }}
                  containerStyle={{ borderBottomWidth: 0 }}
                />
              )}
              keyExtractor={item => item.email}
              ItemSeparatorComponent={this.renderSeparator}
              ListHeaderComponent={this.renderHeader}
              ListFooterComponent={this.renderFooter}
            />
      </SafeAreaView>
      )
    }
}