export default [
    {
      gender: "male",
      name: {
        title: "mr",
        first: "janique",
        last: "costa"
      },
      location: {
        street: "5236 rua tiradentes ",
        city: "bragança",
        state: "alagoas",
        postcode: 81417
      },
      email: "janique.costa@example.com",
      dob: "1969-10-22 10:35:24",
      registered: "2014-09-22 22:38:28",
      phone: "(48) 4518-1459",
      cell: "(22) 3632-3660",
      id: {
        name: "",
        value: null
      },
      picture: {
        large: "https://randomuser.me/api/portraits/men/97.jpg",
        medium: "https://randomuser.me/api/portraits/med/men/97.jpg",
        thumbnail: "https://randomuser.me/api/portraits/thumb/men/97.jpg"
      },
      nat: "BR"
    },
    {
      gender: "female",
      name: {
        title: "miss",
        first: "afşar",
        last: "kavaklıoğlu"
      },
      location: {
        street: "8792 mevlana cd",
        city: "afyonkarahisar",
        state: "kahramanmaraş",
        postcode: 11355
      },
      email: "afşar.kavaklıoğlu@example.com",
      dob: "1974-11-29 01:41:41",
      registered: "2007-12-26 04:00:19",
      phone: "(278)-129-7230",
      cell: "(605)-650-9013",
      id: {
        name: "",
        value: null
      },
      picture: {
        large: "https://randomuser.me/api/portraits/women/78.jpg",
        medium: "https://randomuser.me/api/portraits/med/women/78.jpg",
        thumbnail: "https://randomuser.me/api/portraits/thumb/women/78.jpg"
      },
      nat: "TR"
    },
    {
      gender: "female",
      name: {
        title: "ms",
        first: "eva",
        last: "duncan"
      },
      location: {
        street: "2213 park lane",
        city: "clonmel",
        state: "monaghan",
        postcode: 30411
      },
      email: "eva.duncan@example.com",
      dob: "1985-10-17 13:55:10",
      registered: "2008-10-27 07:12:36",
      phone: "011-429-5904",
      cell: "081-475-3308",
      id: {
        name: "PPS",
        value: "8482495T"
      },
      picture: {
        large: "https://randomuser.me/api/portraits/women/12.jpg",
        medium: "https://randomuser.me/api/portraits/med/women/12.jpg",
        thumbnail: "https://randomuser.me/api/portraits/thumb/women/12.jpg"
      },
      nat: "IE"
    },
    {
      gender: "male",
      name: {
        title: "mr",
        first: "thuy",
        last: "ta"
      },
      location: {
        street: "5236 rua tiradentes ",
        city: "bragança",
        state: "alagoas",
        postcode: 81417
      },
      email: "thuyta@gmail.com",
      dob: "1969-10-22 10:35:24",
      registered: "2014-09-22 22:38:28",
      phone: "(48) 4518-1459",
      cell: "(22) 3632-3660",
      id: {
        name: "",
        value: null
      },
      picture: {
        large: "https://randomuser.me/api/portraits/men/97.jpg",
        medium: "https://randomuser.me/api/portraits/med/men/97.jpg",
        thumbnail: "https://randomuser.me/api/portraits/thumb/men/97.jpg"
      },
      nat: "BR"
    },
    {
      gender: "male",
      name: {
        title: "mr",
        first: "khanh",
        last: "nguyen"
      },
      location: {
        street: "5236 rua tiradentes ",
        city: "bragança",
        state: "alagoas",
        postcode: 81417
      },
      email: "khanhnguyen@gmail.com",
      dob: "1969-10-22 10:35:24",
      registered: "2014-09-22 22:38:28",
      phone: "(48) 4518-1459",
      cell: "(22) 3632-3660",
      id: {
        name: "",
        value: null
      },
      picture: {
        large: "https://randomuser.me/api/portraits/men/97.jpg",
        medium: "https://randomuser.me/api/portraits/med/men/97.jpg",
        thumbnail: "https://randomuser.me/api/portraits/thumb/men/97.jpg"
      },
      nat: "BR"
    },
]