import {
    StyleSheet,
    Dimensions
} from 'react-native'

//import
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import colors from '../../config/colors';
const { height, width } = Dimensions.get('window');
export default styles = StyleSheet.create({

    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    button: {
        width: 100,
        paddingHorizontal: 8,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 5,
    },
    containerStyle: {
        width: 340 / 360 * width,
        height:(1 / 3+0.03) * height,
        marginVertical: 20,
        //backgroundColor: 'transparent',
        backgroundColor: 'white',
        borderRadius: 13,
        elevation: 1,
        alignItems: 'center',
        //  justifyContent: 'center',
    },
    buttonText: {
        textAlign: 'center',
    },
    swiperStyle: {
        height: 1 / 7 * height,
        width: 340 / 360 * width,
        elevation: 2,
        borderTopLeftRadius: 13,
        borderTopRightRadius: 13,
    },
    ViewImage: {
        height: 1 / 7 * height,
        width: 340 / 360 * width,
        elevation: 2,
        borderTopLeftRadius: 13,
        borderTopRightRadius: 13,
        flexDirection:'row'
    },
    //
    wrapper: {
    },
    imageStyle: {
        height: 1 / 7 * height,
        width: 250 / 360 * width,
        // resizeMode:'cover'
    },
    radius:{
        height:50,
        width:50,
        borderRadius:50/2,
        overflow:"hidden",
        backgroundColor:'rgba(0,122,255,0.1)',
        borderWidth:1,
        borderColor:'rgba(0,112,255,0.3)',
        alignItems:'center',
        justifyContent:'center'
    },
    marker:{
        height:20,
        width:20,
        borderWidth:3,
        borderColor:'white',
        borderRadius:20/2,
        overflow:'hidden',
        backgroundColor:'#1A73E8'
    },
    searchBox:{
        top:0,
        position:"absolute",
        width:width
    },
    inputSearch:{
        fontSize:14
    },
    inputWrapper:{
        marginLeft:15,
        marginRight:10,
        marginTop:10,
        marginBottom:0,
        backgroundColor:"#fff",
        opacity:0.9,
        borderRadius:7
    },
});
