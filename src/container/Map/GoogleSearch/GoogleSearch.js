import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet
} from 'react-native';
import RNGooglePlaces from 'react-native-google-places';
export default class GPlacesDemo extends Component {
  openSearchModal() {
    RNGooglePlaces.openAutocompleteModal({
      initialQuery: 'vestar',
      locationRestriction: {
        latitudeSW: 20.9826962,
        longitudeSW: 105.7879711,
        latitudeNE: 21.041487,
        longitudeNE: 105.7718093
      },
      country: 'VN',
      type: 'establishment'
    }, ['placeID', 'location', 'name', 'address', 'types', 'openingHours', 'plusCode', 'rating', 'userRatingsTotal', 'viewport']
    )
      .then((place) => {
        console.log(place);
      })
      .catch(error => console.log(error.message));
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => this.openSearchModal()}
        >
          <Text>Pick a Place</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  button: {
    height: '10%',
    width: '100%'
  }
})