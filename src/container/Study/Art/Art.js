// @flow
import React, { Component } from 'react';
import {
    StyleSheet, View, SafeAreaView, Dimensions, Animated, TextInput, ScrollView, PanResponder,
} from 'react-native';
import Svg, { Path, Defs, LinearGradient, Stop, Ellipse, G, Rect, Circle } from 'react-native-svg';
import * as path from 'svg-path-properties';
import * as shape from 'd3-shape';
import {
    scaleTime,
    scaleLinear,
    scaleQuantile,
} from 'd3-scale';

// const {

// } = Svg;
const d3 = {
    shape,
};

// const height = 800;
const { width, height } = Dimensions.get('window');
const verticalPadding = 5;
const cursorRadius = 10;
const labelWidth = 100;
const data = [
    { x: 150, y: 400 },
    { x: 10, y: 400 },
    { x: 10, y: 200 },
    { x: 50, y: 200 },
    { x: 50, y: 150 },
    { x: 70, y: 100 },
    { x: 130, y: 100 },
    { x: 200, y: 150 },
    { x: 200, y: 230 },
    { x: 210, y: 230 },
    { x: 210, y: 400 },
    { x: 170, y: 400 },
];
data1 = [
    { x: 210, y: 230 },
    { x: 430, y: 230 },
    { x: 430, y: 200 },
    { x: 630, y: 200 },
    { x: 630, y: 450 },
    { x: 210, y: 450 },
    { x: 210, y: 230 },
]
data2 = [
    { x: 10, y: 450 },
    { x: 120, y: 450 },
    { x: 120, y: 430 },
    { x: 120, y: 400 },
    { x: 10, y: 400 },
    { x: 10, y: 450 },
]
data3 = [
    { x: 10, y: 450 },
    { x: 10, y: 650 },
    { x: 100, y: 650 },
    { x: 100, y: 550 },
    { x: 120, y: 550 },
    { x: 120, y: 450 },
    { x: 10, y: 450 },
]
data4 = [
    { x: 100, y: 650 },
    { x: 100, y: 550 },
    { x: 210, y: 550 },
    { x: 210, y: 650 },
    { x: 100, y: 650 },
]
data5 = [
    { x: 210, y: 550 },
    { x: 210, y: 650 },
    { x: 410, y: 650 },
    { x: 410, y: 450 },
    { x: 210, y: 450 },
    { x: 210, y: 500 },
]
data6 = [
    { x: 410, y: 650 },
    { x: 630, y: 650 },
    { x: 630, y: 450 },
    { x: 410, y: 450 },
    { x: 410, y: 650 },
]
function calcDistance(x1, y1, x2, y2) {
    const dx = x1 - x2;
    const dy = y1 - y2;
    return Math.sqrt(dx * dx + dy * dy);
}

function middle(p1, p2) {
    return (p1 + p2) / 2;
}

function calcCenter(x1, y1, x2, y2) {
    return {
        x: middle(x1, x2),
        y: middle(y1, y2),
    };
}
// const scaleX = scaleTime().domain([new Date(2018, 9, 1), new Date(2018, 10, 5)]).range([0, width]);
const scaleX = scaleLinear().domain([0, 100]).range([0, width]);
const scaleY = scaleLinear().domain([0, 300]).range([height - verticalPadding, verticalPadding]);
const scaleLabel = scaleQuantile().domain([0, 300]).range([0, 200, 300]);
const line = d3.shape.line()
    .x(d => d.x)
    .y(d => d.y)
    (data);
const line1 = d3.shape.line()
    .x(d => d.x)
    .y(d => d.y)
    (data1);
const line2 = d3.shape.line()
    .x(d => d.x)
    .y(d => d.y)
    (data2);
const line3 = d3.shape.line()
    .x(d => d.x)
    .y(d => d.y)
    (data3);
const line4 = d3.shape.line()
    .x(d => d.x)
    .y(d => d.y)
    (data4);
const line5 = d3.shape.line()
    .x(d => d.x)
    .y(d => d.y)
    (data5);
const line6 = d3.shape.line()
    .x(d => d.x)
    .y(d => d.y)
    (data6);
// curve(d3.shape.curveBasis)
const properties = path.svgPathProperties(line);
const lineLength = properties.getTotalLength();
class ZoomableSvg extends Component {
    state = {
        zoom: 1,
        left: 0,
        top: 0,
    };

    processPinch(x1, y1, x2, y2) {
        const distance = calcDistance(x1, y1, x2, y2);
        const { x, y } = calcCenter(x1, y1, x2, y2);

        if (!this.state.isZooming) {
            const { top, left, zoom } = this.state;
            this.setState({
                isZooming: true,
                initialX: x,
                initialY: y,
                initialTop: top,
                initialLeft: left,
                initialZoom: zoom,
                initialDistance: distance,
            });
        } else {
            const {
                initialX,
                initialY,
                initialTop,
                initialLeft,
                initialZoom,
                initialDistance,
            } = this.state;

            const touchZoom = distance / initialDistance;
            const dx = x - initialX;
            const dy = y - initialY;

            const left = (initialLeft + dx - x) * touchZoom + x;
            const top = (initialTop + dy - y) * touchZoom + y;
            const zoom = initialZoom * touchZoom;

            this.setState({
                zoom,
                left,
                top,
            });
        }
    }

    processTouch(x, y) {
        if (!this.state.isMoving || this.state.isZooming) {
            const { top, left } = this.state;
            this.setState({
                isMoving: true,
                isZooming: false,
                initialLeft: left,
                initialTop: top,
                initialX: x,
                initialY: y,
            });
        } else {
            const { initialX, initialY, initialLeft, initialTop } = this.state;
            const dx = x - initialX;
            const dy = y - initialY;
            this.setState({
                left: initialLeft + dx,
                top: initialTop + dy,
            });
        }
    }

    componentWillMount() {
        this._panResponder = PanResponder.create({
            onPanResponderGrant: () => { },
            onPanResponderTerminate: () => { },
            onMoveShouldSetPanResponder: () => true,
            onStartShouldSetPanResponder: () => true,
            onShouldBlockNativeResponder: () => true,
            onPanResponderTerminationRequest: () => true,
            onMoveShouldSetPanResponderCapture: () => true,
            onStartShouldSetPanResponderCapture: () => true,
            onPanResponderMove: evt => {
                const touches = evt.nativeEvent.touches;
                const length = touches.length;
                if (length === 1) {
                    const [{ locationX, locationY }] = touches;
                    this.processTouch(locationX, locationY);
                } else if (length === 2) {
                    const [touch1, touch2] = touches;
                    this.processPinch(
                        touch1.locationX,
                        touch1.locationY,
                        touch2.locationX,
                        touch2.locationY
                    );
                }
            },
            onPanResponderRelease: () => {
                this.setState({
                    isZooming: false,
                    isMoving: false,
                });
            },
        });
    }

    render() {
        const viewBoxSize = 65;
        const { height, width } = this.props;
        const { left, top, zoom } = this.state;
        const resolution = viewBoxSize / Math.min(height, width);
        return (
            <View {...this._panResponder.panHandlers}>
                <Svg
                    width={width}
                    height={height}
                    // viewBox="0 0 0 0"
                    preserveAspectRatio="xMinYMin meet">
                    <G
                        transform={{
                            translateX: left * resolution,
                            translateY: top * resolution,
                            scale: zoom,
                        }}>
                        <Path d={line} fill="transparent" stroke="#000" strokeWidth={2} />
                        <Path d={line1} fill="transparent" stroke="#000" strokeWidth={2} />
                        <Path d={line2} fill="transparent" stroke="#000" strokeWidth={2} />
                        <Path d={line3} fill="transparent" stroke="#000" strokeWidth={2} />
                        <Path d={line4} fill="transparent" stroke="#000" strokeWidth={2} />
                        <Path d={line5} fill="transparent" stroke="#000" strokeWidth={2} />
                        <Path d={line6} fill="transparent" stroke="#000" strokeWidth={2} />
                    </G>
                </Svg>
            </View>
        );
    }
}
export default class App extends React.Component {
    componentDidMount() {
    }

    render() {
        return (
            <SafeAreaView style={styles.root}>
                <View style={styles.container}>
                    <ZoomableSvg width={width} height={height} />
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    root: {
        flex: 1,
    },
    container: {
        marginTop: 60,
        height,
        width,
    },
    cursor: {
        width: cursorRadius * 2,
        height: cursorRadius * 2,
        borderRadius: cursorRadius,
        borderColor: '#367be2',
        borderWidth: 3,
        backgroundColor: 'white',
    },
    label: {
        position: 'absolute',
        top: -45,
        left: 0,
        backgroundColor: 'lightgray',
        width: labelWidth,
    },
});