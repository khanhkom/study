import React from 'react';
import { Animated, View, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';

const DEVICE_HEIGHT = Dimensions.get('window').height
const CARD_HEIGHT = 250
const CARD_AMOUNT = 10
const CARD_VISIBLE_HEIGHT = 75
const CARD_VISIBLE_HEIGHT_COLLAPSED = 10
const CARD_VISIBLE_DELTA = CARD_VISIBLE_HEIGHT - CARD_VISIBLE_HEIGHT_COLLAPSED
const TOP_OFFSET = 150
const STACK_HEIGHT = CARD_VISIBLE_DELTA * CARD_AMOUNT - CARD_VISIBLE_DELTA;
const SCROLL_HEIGHT = DEVICE_HEIGHT + CARD_VISIBLE_DELTA * CARD_AMOUNT - CARD_VISIBLE_DELTA

class JuiceCard extends React.Component {

  constructor() {
    super()

    this.offsetY = 0
    this.state = { scrollY: new Animated.Value(0) }
  }

  onPress = e => {
    const offsetY = this.offsetY
    const pressY = e.nativeEvent.pageY
    const currentStackHeight = STACK_HEIGHT - offsetY + CARD_HEIGHT + ( CARD_AMOUNT - 1 ) * CARD_VISIBLE_HEIGHT_COLLAPSED

    let y = TOP_OFFSET

    if (pressY < y || pressY > currentStackHeight + TOP_OFFSET) {
      return false
    }

    const collapsedCount = Math.floor(offsetY / CARD_VISIBLE_DELTA)
    const expanedCount = CARD_AMOUNT - collapsedCount - 2

    for (let i = 0; i <= CARD_AMOUNT; i++) {

      // if a collapsed card
      if (i < collapsedCount) y += CARD_VISIBLE_HEIGHT_COLLAPSED
      // if the last card
      else if (i === CARD_AMOUNT - 1) y += CARD_HEIGHT
      // if a collapsing card
      else if (i === collapsedCount) y += currentStackHeight - CARD_HEIGHT - collapsedCount * CARD_VISIBLE_HEIGHT_COLLAPSED - expanedCount * CARD_VISIBLE_HEIGHT
      // if an expanded card
      else y += CARD_VISIBLE_HEIGHT

      if (pressY < y) {
        return this.setState({ pressed: i })
      }
    }

  }

  render() {
    let { scrollY } = this.state;


    let cardTransform = i => {
      if (!i) return { transform: [{ translateY: TOP_OFFSET }] }

      const translateY = scrollY.interpolate({
        inputRange: [
          -100,
          0,
          CARD_VISIBLE_DELTA * i,
          STACK_HEIGHT,
          STACK_HEIGHT + 100
        ],
        outputRange: [
          TOP_OFFSET + i * 100,
          TOP_OFFSET,
          TOP_OFFSET -CARD_VISIBLE_DELTA * i,
          TOP_OFFSET -CARD_VISIBLE_DELTA * i,
          TOP_OFFSET -CARD_VISIBLE_DELTA * i - i * 5
        ],
      })

      return { transform: [{ translateY }] }
    }

    let cards = [];

    for (let i = 0; i < CARD_AMOUNT; i++) {
      cards.push(
        <Animated.View
          key={`card-${i}`}
          style={[styles.card, cardTransform(i), {
            top: i * CARD_VISIBLE_HEIGHT,
            backgroundColor: i === this.state.pressed ? 'red' : 'white'
          }]}
        ></Animated.View>
      )
    }

    return (
      <View style={{ flex: 1 }}>
        <View style={StyleSheet.absoluteFill}>
          {cards}
        </View>


        <Animated.ScrollView
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: scrollY } } }],
            {
              useNativeDriver: true,
              listener: e => this.offsetY = e.nativeEvent.contentOffset.y
            },

          )}
          scrollEventThrottle={24}
          showsVerticalScrollIndicator={true}
          style={[StyleSheet.absoluteFill, { borderWidth: 3, borderColor: 'red' }]}
        >
          <TouchableOpacity onPress={this.onPress} style={[ { height: SCROLL_HEIGHT, opacity: 0.2, backgroundColor: 'red' } ]}></TouchableOpacity>
        </Animated.ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  card: {
    position: 'absolute',
    borderWidth: 5,
    borderColor: 'black',
    backgroundColor: 'white',
    left: 20,
    right: 20,
    height: CARD_HEIGHT,
    borderRadius: 10,
  },
})

export default JuiceCard
