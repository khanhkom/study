// @flow
import React from "react";
import {
  StatusBar, View, ActivityIndicator, StyleSheet,Text
} from "react-native";
// import { Asset } from "expo";

import { Discovery } from "./index";

const stories = [
  {
    id: "2",
    source: 'https://img2.thuthuatphanmem.vn/uploads/2018/12/25/nhung-hinh-anh-gai-xinh-cuc-dep_012909400.jpg',
    user: "derek.russel",
    avatar: 'https://soikeom88.com/wp-content/uploads/2019/03/hinh-nen-gai-xinh-1429-20.jpg',
  },
  {
    id: "4",
    source: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS3fweBWAWK4j8DmSO_C6PHNOTXfgcajsidVbDWctYtwydJrEN-',
    user: "jmitch",
    avatar: 'https://images.headlines.pw/topnews-2017/imgs/a6/62/a6627618741ad806b564a9b268f8aa8e1c6436d8.jpg',
  },
  {
    id: "7",
    source: 'http://123anhdep.net/wp-content/uploads/2016/03/ngam-nhin-tuyen-chon-hinh-anh-hot-girl-xinh-dep-hot-nhat-facebook-tuan-qua-17.jpg',
    user: "andrea.schmidt",
    avatar: 'http://123anhdep.net/wp-content/uploads/2016/03/ngam-nhin-tuyen-chon-hinh-anh-hot-girl-xinh-dep-hot-nhat-facebook-tuan-qua-17.jpg',
    video: require('../assets/stories/7.mp4'),
  },
  {
    id: "5",
    source:'http://topanhdep.net/wp-content/uploads/2017/05/girl-xinh-mien-tay-13.jpg',
    user: "monicaa",
    avatar:'http://123anhdep.net/wp-content/uploads/2016/03/ngam-nhin-tuyen-chon-hinh-anh-hot-girl-xinh-dep-hot-nhat-facebook-tuan-qua-17.jpg',
  },
  {
    id: "3",
    source: 'http://topanhdep.net/wp-content/uploads/2017/05/girl-xinh-mien-tay-13.jpg',
    user: "alexandergarcia",
    avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTGcFz8YYNrMqo6PIp9tvAWl8SiYf0UwIsI5YGMMroYJSJlvdw5',
  },
  {
    id: "1",
    source:'https://phunusacdep.org/wp-content/uploads/2018/06/doi-mat-cuoi.jpg',
    user: "andrea.schmidt",
    avatar: 'https://3.bp.blogspot.com/-ijjbIpAAJ6U/XJDMMSr2sOI/AAAAAAAALbg/Gdh0k2IrpUghwDfQ2iTyhb2roQl-O-lOgCLcBGAs/s1600/58-anh-gai-xinh-mac-ao-dai-dep%2B%25282%2529.jpg',
  },
  {
    id: "6",
    source:'https://kenh14cdn.com/2016/14-1473656069320.png',
    user: "andrea.schmidt",
    avatar:'https://kenhphunu.com/media/102015/1532451600/datrangbattone.png',
  },
];

type AppState = {
  ready: boolean,
};

export default class App extends React.Component<{}, AppState> {
  state = {
    ready: false,
  };

  async componentDidMount() {
    // await Promise.all(stories.map(story => Promise.all([
    //   Asset.loadAsync(story.source),
    //   Asset.loadAsync(story.avatar),
    //   story.video ? Asset.loadAsync(story.video) : undefined,
    // ])));
    // this.setState({ ready: true });
  }

  render() {
    const { ready } = this.state;
    // if (!ready) {
    //   return (
    //     <View style={styles.container}>
    //       <ActivityIndicator size="large" color="black" />
    //     </View>
    //   );
    // }
    return (
      <View style={{ flex: 1 }}>
        {/* <StatusBar barStyle="light-content" backgroundColor="black" /> */}
        <Discovery {...{ stories }} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
  },
});