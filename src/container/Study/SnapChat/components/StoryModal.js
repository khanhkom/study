// @flow
import * as React from "react";
import { Image, StyleSheet, Dimensions, View } from "react-native";
import Video from "react-native-video";
import { PanGestureHandler, State } from "react-native-gesture-handler";
import Animated from "react-native-reanimated";
const {
  set,
  cond,
  eq,
  add,
  multiply,
  lessThan,
  spring,
  startClock,
  stopClock,
  clockRunning,
  sub,
  defined,
  Value,
  Clock,
  event,
  block,
  and,
  lessOrEq,
  greaterThan,
  call,
  interpolate
} = Animated
import { Story, Position } from "./Story";
type StoryModalProps = {
  story: Story,
  position: Position,
  onRequestClose: () => void,
}
const { width: wWidth, height: wHeight } = Dimensions.get("window");
function runSpring(value, dest, cb) {
  const clock = new Clock()
  const state = {
    finished: new Value(0),
    velocity: new Value(0),
    position: new Value(0),
    time: new Value(0),
  };

  const config = {

    toValue: new Value(0),
    damping: 10,
    mass: 1,
    stiffness: 100,
    overshootClamping: false,
    restSpeedThreshold: 0.001,
    restDisplacementThreshold: 0.001,
  };

  return block([
    cond(clockRunning(clock), 0, [
      set(state.finished, 0),
      set(state.time, 0),
      set(state.velocity, 0),
      set(state.position, value),
      set(config.toValue, dest),
      startClock(clock),
    ]),
    spring(clock, state, config),
    cond(state.finished, stopClock(clock)),
    set(value, state.position),
  ]);
}

export default class StoryModal extends React.PureComponent<StoryModalProps>{
  constructor(props: StoryModalProps) {
    super(props);
    const { x, y, width, height } = props.position;
    this.clock = new Clock();
    this.translateX = new Value(x);
    this.translateY = new Value(y);
    this.width = new Value(width);
    this.height = new Value(height);
    this.velocityY = new Value(0);
    this.state = new Value(State.UNDETERMINED)
    this.onGestureEvent = event([{
      nativeEvent: {
        translationX: this.translateX,
        translationY: this.translateY,
        velocityY: this.velocityY,
        state: this.state,
      }
    }], { useNativeDriver: true }
    )
  }
  render() {
    const { translateX, translateY, width, height, onGestureEvent } = this;
    const { story, position, onRequestClose } = this.props;
    const style = {
      ...StyleSheet.absoluteFillObject,
      width,
      height,
      transform: [
        { translateX },
        { translateY }
      ]
    }
    return (
      <View style={styles.container}>
        <Animated.Code>
          {
            () => block([
                  // runSpring(this.translateX,0),
                  // runSpring(this.translateY, 0),
                  // runSpring(this.width, wWidth),
                  // runSpring(this.height, wHeight)
              cond(eq(this.state, State.UNDETERMINED), runSpring(this.translateX,0)),
              cond(eq(this.state, State.UNDETERMINED), runSpring(this.translateY, 0)),
              cond(eq(this.state, State.UNDETERMINED), runSpring(this.width, wWidth)),
              cond(eq(this.state, State.UNDETERMINED), runSpring(this.height, wHeight)),
              cond(and(eq(this.state, State.END), lessOrEq(this.velocityY, 0)),block( [
                runSpring(this.translateX, 0),
                runSpring(this.translateY, 0),
                runSpring(this.width, wWidth),
                runSpring(this.height, wHeight),
              ])),
              cond(and(eq(this.state, State.END), greaterThan(this.velocityY, 0)), block([
                runSpring(translateX, this.props.position.x),
                runSpring(translateY, this.props.position.y),
                runSpring(width, this.props.position.width),
                runSpring(height, this.props.position.height),
                cond(eq(this.height, this.props.position.height), call([], onRequestClose))
              ])),
              cond(eq(this.state, State.ACTIVE), set(this.width, interpolate(this.translateY, {
                inputRange: [wHeight / 4, wHeight - this.props.position.height],
                // inputRange: [0, wHeight],
                outputRange: [wWidth, this.props.position.width],
              }))),
              cond(eq(this.state, State.ACTIVE), set(this.height, interpolate(this.translateY, {
                inputRange: [wHeight / 4, wHeight - this.props.position.height],
                // inputRange: [0, wHeight],
                outputRange: [wHeight, this.props.position.height],
              }))),
            ])
          }
        </Animated.Code>
        <PanGestureHandler
          activeOffsetY={100}
          onHandlerStateChange={onGestureEvent}
          {...{ onGestureEvent }}
        >
          <Animated.View {...{ style }}>
            {
              !story.video && (
                <Image source={{ uri: story.source }} style={styles.image} />
              )
            }
            {
              story.video && (
                <Video
                  source={story.video}
                  rate={1.0}
                  volume={1.0}
                  isMuted={false}
                  resizeMode="cover"
                  shouldPlay
                  isLooping
                  style={styles.video}
                />
              )
            }
          </Animated.View>
        </PanGestureHandler>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    // backgroundColor: 'green'
  },
  image: {
    ...StyleSheet.absoluteFillObject,
    width: null,
    height: null,
    borderRadius: 5,
  },
  video: {
    ...StyleSheet.absoluteFillObject,
    borderRadius: 5,
  },
});