import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    Image,
    Dimensions
} from 'react-native';
import HeaderReal from '../../../common/HeaderReal';
import { WIDTH, HEIGHT } from '../../../config/Function';
import STYLES from '../../../config/styles.config';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
const { width, height } = Dimensions.get('window');
import { Table, TableWrapper, Row, Rows, Col } from 'react-native-table-component';
export default class XemDiem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tableHead: ['Tên Môn', 'Số Tc', 'Điểm Bộ Phận', 'Điểm Cuối Phần', 'Điểm Trung Bình','Đánh giá'],
            tableTitle: ['Tiết 1', 'Tiết 2', 'Tiết 3', 'Tiết 4', 'Tiết 5', 'Tiết 6', 'Tiết 7', 'Tiết 8', 'Tiết 9', 'Tiết 10', 'Tiết 11', 'Tiết 12',],
            tableData: [
                ['Lập trình hướng đối tượng', '2', '8.3', '8.0', '8.2','Đạt'],
                ['Internet và giao thức', '3', '7.6', '8.5', '8.3','Đạt'],
                ['Cơ sở an toàn thông tin', '2', '7.8', '7.9', '7.9','Đạt'],
                ['Quản trị mạng', '3', '8.5', '7.0', '7.7', 'Đạt'],
                ['Cơ sở dữ liệu', '3', '8.3', '7.5', '7.8', 'Đạt'],
                ['Cơ sở an toàn thông tin', '2', '9.0', '9.2', '9.1','Đạt'],
            ]
        }
    }
    render() {
        const state = this.state;
        return (
            <View>
                <HeaderReal
                    onButton={() => this.props.navigation.goBack()}
                    title={'Điểm Thi'} />
                <ScrollView showsHorizontalScrollIndicator={false}
                    scrollEventThrottle={16}>
                    <View style={styles.body}>
                        <View style={styles.header}>
                            <View style={styles.headerchild1}>
                                <AntDesign name="tags" size={WIDTH(10)} color="#000" />
                                <Text style={{ fontSize: STYLES.fontSizeLabel, color: '#000' }}>Thông Tin Điểm</Text>
                            </View>
                            <View style={styles.headerchild2}>
                                <Image source={{ uri: 'https://cdn.tuoitre.vn/thumb_w/586/2019/4/11/ds-12-thi-sinh-con-em-nganh-giao-duc-gian-lan-diem-thpt-2018-1554955195674241435083.png' }} style={styles.image}>

                                </Image>
                                <View style={styles.choiceView}>
                                    <View style={{ flexDirection: 'row', }}>
                                        <Text style={{ color: '#000', marginRight: WIDTH(10), width: WIDTH(60) }}>Chọn học kỳ</Text>
                                        <View style={styles.box}>
                                            <Text style={styles.textchoice}>Học kỳ 1 - Năm học 2019-2020</Text>
                                            <Entypo name="triangle-down" size={WIDTH(10)} color="#000" />
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: 'row', marginTop: HEIGHT(5) }}>
                                        <Text style={{ color: '#000', marginRight: WIDTH(10), marginRight: WIDTH(10), width: WIDTH(60) }}>Lọc</Text>
                                        <View style={styles.box}>
                                            <Text style={styles.textchoice}>Học phần đã có điểm</Text>
                                            <Entypo name="triangle-down" size={WIDTH(10)} color="#000" />
                                        </View>
                                    </View>
                                    <View style={{ width: WIDTH(250), marginTop: HEIGHT(5) }}>
                                        <Text style="#000">Lưu ý mọi thắc mắc của sinh viên có thể gửi về phòng đào tạo</Text>
                                        <Text>...</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={styles.viewTkb}>
                            <View style={{ flexDirection: 'row', marginTop: HEIGHT(10), justifyContent: 'center', width: WIDTH(350) }}>
                                <Text>Mã số: </Text>
                                <Text style={{ color: '#1874CD' }}>B16DCTV</Text>
                                <Text> - Họ Tên: </Text>
                                <Text style={{ color: '#1874CD' }}>Phạm Văn Huấn - Ngày sinh:26/09/1998</Text>
                            </View>
                            <View style={styles.boxTKB}>
                                <ScrollView horizontal={true}>
                                    <View>
                                        <Table borderStyle={{ borderColor: '#C1C0B9' }}>
                                            <Row data={state.tableHead} widthArr={[ WIDTH(90), WIDTH(60), WIDTH(90), WIDTH(90), WIDTH(90), WIDTH(90),]} style={styles.head} textStyle={styles.text} />
                                        </Table>
                                        <ScrollView style={styles.dataWrapper}>
                                            <Table borderStyle={{ borderColor: '#C1C0B9' }}>
                                                {
                                                    state.tableData.map((rowData, index) => (
                                                        <Row
                                                            key={index}
                                                            data={rowData}
                                                            widthArr={[ WIDTH(90), WIDTH(60), WIDTH(90), WIDTH(90), WIDTH(90), WIDTH(90),]}
                                                            style={[styles.row, index % 2 && { backgroundColor: '#F7F6E7' }]}
                                                            textStyle={styles.text}
                                                        />
                                                    ))
                                                }
                                            </Table>
                                        </ScrollView>
                                    
                                    </View>
                                </ScrollView>
                            </View>
                        </View>
                    </View>
                    <View style={{ height: HEIGHT(64) }}>
                                        </View>
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    body: {
        justifyContent: 'center',
        padding: WIDTH(5)
    },
    header: {
        borderWidth: 1,
        borderColor: '#ddd',

    },
    headerchild1: {
        backgroundColor: '#66CCFF',
        paddingTop: HEIGHT(10),
        paddingBottom: HEIGHT(10),
        paddingLeft: WIDTH(10),
        flexDirection: 'row',
        alignItems: 'center'
    },
    headerchild2: {
        // backgroundColor: '#66CCFF',
        paddingTop: HEIGHT(10),
        paddingBottom: HEIGHT(10),
        paddingLeft: WIDTH(10),
        flexDirection: 'row',
        alignItems: 'center'
    },
    image: {
        height: HEIGHT(60),
        width: HEIGHT(60),

    },
    choiceView: {
        flex: 0,
        paddingLeft: WIDTH(10)
    },
    box: {
        borderWidth: 1,
        borderColor: '#aaa',
        paddingLeft: WIDTH(1),
        paddingRight: WIDTH(1),
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: WIDTH(3)
    },
    textchoice: {
        marginRight: WIDTH(10),
        color: '#000'
    },
    viewTkb: {
        // height: HEIGHT(400),
        borderColor: '#ddd',
        borderWidth: 1,
        borderTopColor: '#000',
        borderTopWidth: 2,
        alignItems: 'center'
    },
    boxTKB: {
        width: WIDTH(350),
        marginTop: HEIGHT(5),
        borderColor: '#ddd',
        borderWidth: 1
    },
    head: { height: HEIGHT(50), backgroundColor: '#c8e1ff' },
    text: { textAlign: 'center', fontWeight: '100' },
    dataWrapper: { marginTop: 0 },
    row: { height: HEIGHT(45), backgroundColor: '#E7E6E1' },
    title: { flex: 1, backgroundColor: '#f6f8fa', width: WIDTH(40) },
})