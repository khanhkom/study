import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    Image,
    Dimensions
} from 'react-native';
import HeaderReal from '../../../common/HeaderReal';
import { WIDTH, HEIGHT } from '../../../config/Function';
import STYLES from '../../../config/styles.config';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
const { width, height } = Dimensions.get('window');
import { Table, TableWrapper, Row, Rows, Col } from 'react-native-table-component';
export default class XemTKB extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tableHead: ['', 'Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7', 'Chủ Nhật'],
            tableTitle: ['Tiết 1', 'Tiết 2', 'Tiết 3', 'Tiết 4', 'Tiết 5', 'Tiết 6', 'Tiết 7', 'Tiết 8', 'Tiết 9', 'Tiết 10', 'Tiết 11', 'Tiết 12',],
            tableData: [
                ['Công nghệ truyền tải quang', '', '', '', 'Báo hiệu và điều khiển kết nối', '', ''],
                ['', 'Internet và giao thức', '', '', 'Đa truy nhập vô tuyến', '', ''],
                ['Cơ sở an toàn thông tin', '', 'Lập trình mạng', '', '', '', ''],
                ['', 'Quản trị mạng', '', '', '', '', 'Lập trình PHP'],
                ['Cơ sở dữ liệu', '', '', '', '', 'Lập trình mạng', ''],
                ['', 'Cơ sở an toàn thông tin', '', 'Đa truy nhập vô tuyến', '', '', ''],
            ]
        }
    }
    render() {
        const state = this.state;
        return (
            <View>
                <HeaderReal
                    onButton={() => this.props.navigation.goBack()}
                    title={'Thời Khóa Biểu'} />
                <ScrollView showsHorizontalScrollIndicator={false}
                    scrollEventThrottle={16}>
                    <View style={styles.body}>
                        <View style={styles.header}>
                            <View style={styles.headerchild1}>
                                <AntDesign name="tags" size={WIDTH(10)} color="#000" />
                                <Text style={{ fontSize: STYLES.fontSizeLabel, color: '#000' }}>Thông Tin Thời Khóa Biểu</Text>
                            </View>
                            <View style={styles.headerchild2}>
                                <Image source={{ uri: 'https://i.pinimg.com/236x/71/e7/33/71e7333b23c8bc2e218466d3b7a0ec05.jpg' }} style={styles.image}>

                                </Image>
                                <View style={styles.choiceView}>
                                    <View style={{ flexDirection: 'row', }}>
                                        <Text style={{ color: '#000', marginRight: WIDTH(10) }}>Chọn học kỳ xem TKB</Text>
                                        <View style={styles.box}>
                                            <Text style={styles.textchoice}>Học kỳ 1 - Năm học 2019-2020</Text>
                                            <Entypo name="triangle-down" size={WIDTH(10)} color="#000" />
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: 'row', marginTop: HEIGHT(5) }}>
                                        <View style={styles.box}>
                                            <Text style={styles.textchoice}>TKB theo tuần</Text>
                                            <Entypo name="triangle-down" size={WIDTH(10)} color="#000" />
                                        </View>
                                        <View style={styles.box}>
                                            <Text style={styles.textchoice}>Tuần 02 [19/08/2019-25/08/2019]</Text>
                                            <Entypo name="triangle-down" size={WIDTH(10)} color="#000" />
                                        </View>
                                    </View>
                                    <View style={{ width: WIDTH(250), marginTop: HEIGHT(5) }}>
                                        <Text style="#000">Lưu ý tuần 1 tương ứng với tuần 1 của học kỳ, bắt đầu từ ngày 12/08/2019</Text>
                                        <Text>...</Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={styles.viewTkb}>
                            <View style={{ flexDirection: 'row', marginTop: HEIGHT(10), justifyContent: 'center', width: WIDTH(350) }}>
                                <Text>Mã số: </Text>
                                <Text style={{ color: '#1874CD' }}>B16DCTV</Text>
                                <Text> - Họ Tên: </Text>
                                <Text style={{ color: '#1874CD' }}>Phạm Văn Huấn - Ngày sinh:26/09/1998</Text>
                            </View>
                            <View style={styles.boxTKB}>
                                <ScrollView horizontal={true}>
                                    <View>
                                        <Table borderStyle={{ borderColor: '#C1C0B9' }}>
                                            <Row data={state.tableHead} widthArr={[WIDTH(40), WIDTH(90), WIDTH(90), WIDTH(90), WIDTH(90), WIDTH(90), WIDTH(90), WIDTH(90),]} style={styles.head} textStyle={styles.text} />
                                        </Table>
                                        <ScrollView style={styles.dataWrapper}>
                                            <Table borderStyle={{ borderColor: '#C1C0B9' }}>
                                                <TableWrapper style={{ flexDirection: 'row' }}>
                                                    <Col data={state.tableTitle} style={styles.title} heightArr={[HEIGHT(30), HEIGHT(30), HEIGHT(30), HEIGHT(30), HEIGHT(30), HEIGHT(30), HEIGHT(30), HEIGHT(30), HEIGHT(30), HEIGHT(30), HEIGHT(30), HEIGHT(30)]} textStyle={styles.text} />
                                                    <Rows
                                                        data={state.tableData}
                                                        widthArr={[WIDTH(90), WIDTH(90), WIDTH(90), WIDTH(90), WIDTH(90), WIDTH(90), WIDTH(90),]}
                                                        style={[styles.row,]}
                                                        textStyle={[styles.text,{color:'#006400'}]}
                                                    />
                                                </TableWrapper>
                                            </Table>
                                        </ScrollView>
                                    </View>
                                </ScrollView>
                            </View>
                        </View>
                    </View>
                    <View style={{ height: HEIGHT(64) }}>

</View>
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    body: {
        justifyContent: 'center',
        padding: WIDTH(5)
    },
    header: {
        borderWidth: 1,
        borderColor: '#ddd',

    },
    headerchild1: {
        backgroundColor: '#66CCFF',
        paddingTop: HEIGHT(10),
        paddingBottom: HEIGHT(10),
        paddingLeft: WIDTH(10),
        flexDirection: 'row',
        alignItems: 'center'
    },
    headerchild2: {
        // backgroundColor: '#66CCFF',
        paddingTop: HEIGHT(10),
        paddingBottom: HEIGHT(10),
        paddingLeft: WIDTH(10),
        flexDirection: 'row',
        alignItems: 'center'
    },
    image: {
        height: HEIGHT(60),
        width: HEIGHT(60),

    },
    choiceView: {
        flex: 0,
        paddingLeft: WIDTH(10)
    },
    box: {
        borderWidth: 1,
        borderColor: '#aaa',
        paddingLeft: WIDTH(1),
        paddingRight: WIDTH(1),
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: WIDTH(3)
    },
    textchoice: {
        marginRight: WIDTH(10),
        color: '#000'
    },
    viewTkb: {
        // height: HEIGHT(400),
        borderColor: '#ddd',
        borderWidth: 1,
        borderTopColor: '#000',
        borderTopWidth: 2,
        alignItems: 'center'
    },
    boxTKB: {
        width: WIDTH(350),
        marginTop: HEIGHT(5),
        borderColor: '#ddd',
        borderWidth: 1
    },
    head: { height: HEIGHT(40), backgroundColor: '#c8e1ff' },
    text: { textAlign: 'center', fontWeight: '100' },
    dataWrapper: { marginTop: 0 },
    row: { height: HEIGHT(60), backgroundColor: '#E7E6E1' },
    title: { flex: 1, backgroundColor: '#f6f8fa', width: WIDTH(40) },
})