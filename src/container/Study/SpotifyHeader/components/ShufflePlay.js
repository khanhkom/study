import * as React from "react";
import {
  View, Text, StyleSheet, TouchableWithoutFeedback,
} from "react-native";
import STYLES from "../../../../config/styles.config";

export const BUTTON_HEIGHT = 48*STYLES.heightScreen/640;
export const BUTTON_WIDTH = 160*STYLES.widthScreen/640;

export default () => (
  <TouchableWithoutFeedback>
    <View style={styles.button}>
      {/* <Text style={styles.label}>PLAY</Text> */}
    </View>
  </TouchableWithoutFeedback>
);

const styles = StyleSheet.create({
  button: {
    // alignSelf: "center",
    backgroundColor: "#1ed760",
    height: BUTTON_WIDTH,
    width: BUTTON_WIDTH,
    borderRadius: BUTTON_WIDTH/2,
    // justifyContent: "center",
    marginLeft:STYLES.widthScreen*10/360
  },
  label: {
    color: "white",
    fontSize: 14,
    textAlign: "center",
    fontWeight: "600",
  },
});