import * as React from "react";
import {
  StyleSheet, View,
} from "react-native";
import  LinearGradient  from "react-native-linear-gradient";
import Animated from "react-native-reanimated";
import { onScroll } from "react-native-redash";

import {
  Album, MAX_HEADER_HEIGHT, MIN_HEADER_HEIGHT,
} from "./Model";
import Track from "./Track";
import ShufflePlay, { BUTTON_HEIGHT, BUTTON_WIDTH } from "./ShufflePlay";
import Header from "./Header";
import STYLES from "../../../../config/styles.config";

interface ContentProps {
  album: Album;
  y: Animated.Value<number>;
}

const {
  interpolate, Extrapolate,
} = Animated;

export default ({ album: { artist, tracks }, y }: ContentProps) => {
  const height = interpolate(y, {
    inputRange: [-MAX_HEADER_HEIGHT, -BUTTON_WIDTH / 2],
    outputRange: [0, MAX_HEADER_HEIGHT + BUTTON_WIDTH],
    extrapolate: Extrapolate.CLAMP,
  });
  const opacity = interpolate(y, {
    inputRange: [-MAX_HEADER_HEIGHT / 2, 0, MAX_HEADER_HEIGHT / 2],
    outputRange: [0, 1, 0],
    extrapolate: Extrapolate.CLAMP,
  });
  return (
    <Animated.ScrollView
      onScroll={onScroll({ y })}
      style={styles.container}
      showsVerticalScrollIndicator={false}
      scrollEventThrottle={1}
      stickyHeaderIndices={[1]}
    >
      <View style={styles.cover}>
        <Animated.View
          style={[styles.gradient, { height }]}
        >
          <LinearGradient
            style={StyleSheet.absoluteFill}
            start={{x:0,y: 0.4}}
            end={{x:0,y: 1}}
            colors={['transparent', 'lightskyblue']}
          />
        </Animated.View>
        <View style={styles.artistContainer}>
          <Animated.Text style={[styles.artist, { opacity }]}>{''}</Animated.Text>
        </View>
      </View>
      <View style={styles.header}>
        <Header {...{ y, artist }} />
        <ShufflePlay {...{y}}/>
      </View>
      <View style={styles.tracks}>
        {
          tracks.map((track, key) => (
            <Track
              index={key + 1}
              {...{ track, key, artist }}
            />
          ))
        }
      </View>
      <View style={{height:200}}>

      </View>
    </Animated.ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: MIN_HEADER_HEIGHT - BUTTON_WIDTH / 3,

  },
  cover: {
    height: MAX_HEADER_HEIGHT - BUTTON_HEIGHT,
  },
  gradient: {
    position: "absolute",
    left: 0,
    bottom: -5,
    right: 0,
    alignItems: "center",
  },
  artistContainer: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: "center",
    alignItems: "center",
  },
  artist: {
    textAlign: "center",
    color: "white",
    fontSize: 48,
    fontWeight: "bold",
  },
  header: {
    marginTop: -BUTTON_WIDTH,
  },
  tracks: {
    paddingTop: 14*STYLES.heightScreen/640,
    padding: 6*STYLES.heightScreen/640,
    backgroundColor: "#fff",
  },
});