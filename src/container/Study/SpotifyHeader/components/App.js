import React, { useState, useEffect } from "react";
// import { Asset } from "expo-asset";
import { StatusBar } from "react-native";
// import { AppLoading } from "expo";

import Album from "./Album";
import { Album as AlbumModel } from "./Model";

const album: AlbumModel = {
  name: "Remote Control",
  artist: "Khanh",
  release: 2016,
  // eslint-disable-next-line global-require
  cover: 'https://2.bp.blogspot.com/-fjf5yU5r1Jk/WE1VD1BBKpI/AAAAAAAAjgI/bXwGoigAPJYvScMPtzJtzbOJfoGQO2C_ACEw/s1600/15349541_533868826819201_3350340522319981193_n.jpg',
  tracks: [
    { name: "Họ và tên", artist: "Họ và tên" },
    { name: "Tuổi",artist:'Tuổi' },
    { name: "Email",artist:"Email" },
    { name: "Họ và tên", artist: "Họ và tên" },
    { name: "Tuổi",artist:'Tuổi' },
    { name: "Email",artist:"Email" },
    { name: "Họ và tên", artist: "Họ và tên" },
    { name: "Tuổi",artist:'Tuổi' },
    { name: "Email",artist:"Email" },
    { name: "Họ và tên", artist: "Họ và tên" },
    { name: "Tuổi",artist:'Tuổi' },
    { name: "Email",artist:"Email" },
  ],
};

export default () => {
//   const [ready, setReady] = useState(false);
//   useEffect(() => {
//     (async () => {
//       await Asset.loadAsync(album.cover);
//       setReady(true);
//     })();
//   });
//   if (!ready) {
//     return <AppLoading />;
//   }
  return (
    <>
      {/* <StatusBar barStyle="light-content" /> */}
      <Album {...{ album }} />
    </>
  );
};