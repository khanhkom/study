import * as React from "react";
import { StyleSheet } from "react-native";
import Animated from "react-native-reanimated";
import { MIN_HEADER_HEIGHT, HEADER_DELTA } from "./Model";
import { BUTTON_HEIGHT ,BUTTON_WIDTH} from "./ShufflePlay";
import STYLES from "../../../../config/styles.config";

interface HeaderProps {
  artist: string;
  y: Animated.Value<number>;
}

const { interpolate, Extrapolate } = Animated;

export default ({ artist, y }: HeaderProps) => {
  const opacity = interpolate(y, {
    inputRange: [HEADER_DELTA - 18*STYLES.heightScreen/640, HEADER_DELTA+10*STYLES.heightScreen/640],
    outputRange: [0, 1],
    extrapolate: Extrapolate.CLAMP,
  });
  const textOpacity = interpolate(y, {
    inputRange: [HEADER_DELTA - 8, HEADER_DELTA - 4],
    outputRange: [0, 1],
    extrapolate: Extrapolate.CLAMP,
  });
  return (
    <Animated.View style={[styles.container, { opacity }]}>
      <Animated.Text style={[styles.title, { opacity: textOpacity }]}>{'Nguyễn Hữu Khánh'}</Animated.Text>
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    top: BUTTON_WIDTH / 2 - MIN_HEADER_HEIGHT-20*STYLES.heightScreen/640,
    left: 0,
    right: 0,
    height: MIN_HEADER_HEIGHT,
    backgroundColor: "lightskyblue",
    paddingTop: 70*STYLES.heightScreen/640,
  },
  title: {
    color: "white",
    fontSize: 16,
    textAlign: "center",
    fontWeight: "400",
  },
});