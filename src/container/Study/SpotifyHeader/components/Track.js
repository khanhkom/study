import * as React from "react";
import { View, Text, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/Feather";

import { Track } from "./Model";
import STYLES from "../../../../config/styles.config";

interface TrackProps {
  track: Track;
  artist: string;
  index: number;
}

export default ({ track, artist, index }: TrackProps) => (
  <View style={styles.container} key={index}>
    <View style={styles.item}>
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <Text style={styles.label}>Họ và tên</Text>
      </View>
      <View style={{justifyContent:'center'}}>
        <Text style={styles.info}>Nguyễn Hữu Khánh</Text>
      </View>
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    paddingLeft: STYLES.widthScreen * 10 / 640,
  },
  item: {
    borderBottomColor: '#ddd',
    borderBottomWidth: 1,
    paddingTop: STYLES.heightScreen * 10 / 640,
    paddingBottom: STYLES.heightScreen * 10 / 640,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 10 * STYLES.widthScreen / 640,
    paddingRight: STYLES.widthScreen * 20 / 640
  },
  label: {
    color: '#000',
    fontSize: STYLES.fontSizeNormalText
  },
  info:{
    color:'#000',
    fontSize: STYLES.fontSizeNormalText
  }
}
);