import React from 'react';
// import { Asset, AppLoading } from 'expo';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity
} from 'react-native';
import STYLES from '../../../config/styles.config';
const mariner = '#3B5F8F';
const mediumPurple = '#8266D4';
const tomato = '#F95B57';
const mySin = '#F3A646';
import HeaderReal from '../../../common/HeaderReal';
import Ionicons from 'react-native-vector-icons/Ionicons';

import ItemSmall from '../../../common/ItemSmall';
import { WIDTH } from '../../../config/Function';
const sections: Section[] = [
    {
        CapHienTai: [
            {
                title: 'Thống kê lịch làm việc',
                leftColor: mediumPurple,
                rightColor: mariner,
                nhiemvu: [{ ten: 'Số nhiệm vụ đã hoàn thành đúng hạn theo tuần, tháng, năm.', loai: 2 },
                { ten: 'Số nhiệm vụ đã hoàn thành không đúng hạn theo tuần, tháng, năm', loai: 2 },
                { ten: 'Số nhiệm vụ chưa hoàn thành theo tuần, tháng, năm.', loai: 2 },
                { ten: 'Đảng viên xử lý nhiều nhiệm vụ nhất theo tuần, tháng, năm.', loai: 2 },
                { ten: 'Đảng viên hoàn thành đúng hạn nhiều nhiệm vụ nhất theo tuần, tháng, năm.', loai: 2 }
                ]
            },
            {
                title: 'Thống kê về sinh hoạt chi bộ, đảng bộ',
                leftColor: tomato,
                rightColor: mediumPurple,
                nhiemvu: [{ ten: 'Số cuộc họp/sinh hoạt đã được tổ chức theo trong tuần, tháng, năm.', loai: 2 },
                { ten: 'Số cuộc họp theo kế hoạch đã được tổ chức theo tuần, tháng, năm.', loai: 2 },
                { ten: 'Số cuộc họp không theo kế hoạch đã được tổ chức theo tuần, tháng, năm.', loai: 2 },
                { ten: 'Điểm trung bình của các cuộc họp đã được tổ chức.', loai: 5 },
                ]
            },
            {
                title: 'Thống kê thông tin đảng viên ',
                leftColor: mySin,
                rightColor: tomato,
                nhiemvu: [{ ten: 'Tổng số đảng viên', loai: 3 },
                { ten: 'Số đảng viên chính thức', loai: 3 },
                { ten: 'Số đảng viên dự bị', loai: 3 },
                { ten: 'Số đảng viên kết nạp lại', loai: 3 },
                ]
            },
            {
                title: 'Đánh giá chất lượng họp chi bộ ',
                leftColor: mediumPurple,
                rightColor: tomato,
                nhiemvu: [{ ten: 'Số lượng đảng viên/danh sách vắng theo từng cuộc họp.', loai: 1 },
                { ten: 'Số lượng đảng viên vắng trung bình theo tuần, tháng, năm.', loai: 2 },
                { ten: 'Đảng viên vắng nhiều cuộc họp nhất theo tuần, tháng, năm.', loai: 2 },
                ]
            },
        ],
        CapDuoi: [{
            title: 'Thống kê về cơ cấu tổ chức, con người',
            leftColor: mediumPurple,
            rightColor: mariner,
            nhiemvu: [{ ten: 'Cây tổ chức cấp dưới/danh sách cấp dưới', loai: 4 },
            { ten: 'Thống kê số lượng đảng viên cấp dưới theo 4 tiêu chí', loai: 3 },
            ]
        },
        {
            title: 'Đánh giá chất lượng họp của cấp dưới',
            leftColor: tomato,
            rightColor: mediumPurple,
            nhiemvu: [{ ten: 'Thống kê điểm sinh hoạt trung bình do cấp dưới tự đánh giá.', loai: 5 },
            { ten: 'Thống kê số lượng đảng viên vắng sinh hoạt trung bình', loai: 3 },
            { ten: 'Số cuộc họp đã được tổ chức bởi các cấp dưới', loai: 3 },
            { ten: 'Số văn bản, nghị quyết đã được ban hành', loai: 3 },
            ]
        },]
    }
];

export default class ThongKe extends React.Component<{}, AppState> {
    render() {
        return (
            <View>
                <HeaderReal
                    onButton={() => this.props.navigation.goBack()}
                    title={'Thống kê'} />
                <View style={styles.body}>
                    <ItemSmall
                        icon={<Ionicons size={WIDTH(30)} name={'ios-copy'} />}
                        key={1}
                        title={'Thống kê tại cơ sở Đảng'}
                        describe={'Gồm những thông tin về lịch làm việc, họp/sinh hoạt chi bộ, đảng bộ, thông tin đảng viên và Đánh giá chất lượng họp chi bộ.'}
                        onButton={() => this.props.navigation.navigate('ReportLv1', { sections: sections[0].CapHienTai, title: 'Thống kê tại cơ sở Đảng' })}
                    />
                    <ItemSmall
                        icon={<Ionicons size={WIDTH(30)} name={'ios-git-branch'} />}
                        key={1}
                        title={'Thống kê, đánh giá các cơ sở cấp dưới'}
                        describe={'Gồm các thông tin về cơ cấu tổ chức, con người, đánh giá chất lượng họp/sinh hoạt của cấp dưới.'}
                        onButton={() => this.props.navigation.navigate('ReportLv1', { sections: sections[0].CapDuoi, title: 'Thống kê, đánh giá các cơ sở Đảng cấp dưới' }) }
                    />
                </View>
            </View >
        );
    }
}
const styles = StyleSheet.create({
    body: {
        paddingTop: 5 * STYLES.heightScreen / 640,
        flex: 0,
        // paddingTop: 10 * STYLES.heightScreen / 640,
        height: STYLES.heightScreen,
        backgroundColor: '#eee'
    },
    item: {
        paddingBottom: 13 * STYLES.heightScreen / 640,
        paddingTop: 13 * STYLES.heightScreen / 640,
        borderBottomColor: '#ddd',
        borderTopColor: '#ddd',
        backgroundColor: '#fff',
        borderBottomWidth: 1,
        borderTopWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 10 * STYLES.widthScreen / 360,
        paddingRight: 10 * STYLES.widthScreen / 360,
        justifyContent: 'space-between',
        marginTop: 5 * STYLES.heightScreen / 640
    },
    text: {
        fontSize: STYLES.fontSizeNormalText,
        color: '#000',
        width: STYLES.widthScreen * 0.8,
        marginLeft: 10 * STYLES.widthScreen / 360
    }
})