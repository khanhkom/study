// @flow
import * as React from 'react';
import {
  View, Image, Text, StyleSheet,
} from 'react-native';
import STYLES from '../../../config/styles.config';
import AntDesign from 'react-native-vector-icons/AntDesign'
type MockEntryProps = {
  image: string,
};

export default class Page extends React.PureComponent<MockEntryProps> {
  componentDidMount(){
    console.log(this.props.nhiemvu)
  }
  render() {
    const { nhiemvu, soluong, image: source } = this.props
    return (
      <View style={styles.container}>
        {nhiemvu.map((item, index) => (
          <View style={styles.rightCell} key={index}>
            <View style={{ flexDirection: 'row' }}>
              <AntDesign name="right" size={20} />
              <Text style={{width:STYLES.widthScreen*0.9, fontSize: STYLES.fontSizeSubText, marginLeft: 10 * STYLES.widthScreen / 360 ,color:'#000'}}>{item.ten}</Text>
            </View>
            {item.loai === 2 ?
              <View style={{ flexDirection: 'row' }}>
                <Text style={styles.subtitle}>Theo tuần :</Text>
                <Text style={styles.soluong}>{soluong}</Text>
                <Text style={styles.subtitle}>Theo tháng :</Text>
                <Text style={styles.soluong}>{soluong}</Text>
                <Text style={styles.subtitle}>Theo năm :</Text>
                <Text style={styles.soluong}>{soluong}</Text></View> :

              <View>
                <Text style={styles.tendv}>Nguyễn Khánh</Text>
              </View>}
          </View>
        ))}

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 10 * STYLES.heightScreen / 640,
    paddingTop: 20 * STYLES.heightScreen / 640
    // flexDirection: 'row',
  },
  leftCell: {
    padding: 8,
  },
  rightCell: {
    paddingVertical: 8,
    paddingRight: 8,
    justifyContent: 'center',
    width: STYLES.widthScreen
  },
  image: {
    width: 45,
    height: 45,
    borderRadius: 5,
  },
  subtitle: {
    color:'#000',
    marginLeft: 25 * STYLES.widthScreen / 360,
    marginTop: 5 * STYLES.widthScreen / 360,
  },
  soluong:{
    color: 'gray',
    marginLeft: 5 * STYLES.widthScreen / 360,
    marginTop: 5 * STYLES.widthScreen / 360,
  },
  tendv:{
    marginLeft: 25 * STYLES.widthScreen / 360,
    marginTop: 5 * STYLES.widthScreen / 360,
    // color:'#000'
  }
});