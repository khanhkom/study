// @flow
import * as React from 'react';
import {
    View, Image, Text, StyleSheet, TouchableOpacity,
    ScrollView
} from 'react-native';
import STYLES from '../../../config/styles.config';
import Entypo from 'react-native-vector-icons/Entypo';
import HeaderReal from '../../../common/HeaderReal'
import { HEIGHT, WIDTH } from '../../../config/Function';
import { Container, Header, Content, Accordion } from "native-base";
const dataArray = [
    { title: "Họp thường kỳ tháng 1", content: "Lorem ipsum dolor sit amet" },
    { title: "Họp thường kỳ tháng 2", content: "Lorem ipsum dolor sit amet" },
];
export default class ReportLv2 extends React.Component {
    componentDidMount() {
        const nhiemvu = this.props.navigation.getParam('nhiemvu')
        console.log('sssss')
        console.log(nhiemvu)
    }
    showCayToChuc = (id) => {
        if (id === 4) {

        }
    }
    _renderHeader(item, expanded) {
        return (
          <View style={{
            flexDirection: "row",
            padding: 10,
            justifyContent: "space-between",
            alignItems: "center" ,
            backgroundColor: "#A9DAD6" }}>
          <Text style={{ fontWeight: "600" }}>
              {" "}{item.title}
            </Text>
            {expanded
              ? <Icon style={{ fontSize: 18 }} name="remove-circle" />
              : <Icon style={{ fontSize: 18 }} name="add-circle" />}
          </View>
        );
      }
      _renderContent(item) {
        return (
          <Text
            style={{
              backgroundColor: "#e3f1f1",
              padding: 10,
              fontStyle: "italic",
            }}
          >
            {item.content}
          </Text>
        );
      }
    render() {
        const nhiemvu = this.props.navigation.getParam('nhiemvu')
        const soluong = 12
        return (
            <View>
                <HeaderReal
                    onButton={() => this.props.navigation.goBack()}
                    title={nhiemvu.title} />

                <ScrollView
                    showsHorizontalScrollIndicator={false}
                    scrollEventThrottle={16}>
                    <View style={styles.container}>
                        {nhiemvu.nhiemvu.map((item, index) => (
                            <TouchableOpacity onPress={() => this.showCayToChuc(item.loai)} style={styles.rightCell} key={index}>
                                <View>
                                    <View style={{ flexDirection: 'row', paddingBottom: HEIGHT(4) }}>
                                        {/* <AntDesign name="right" size={20} /> */}
                                        <Text style={{ width: STYLES.widthScreen * 0.9, fontSize: STYLES.fontSizeNormalText, marginLeft: 10 * STYLES.widthScreen / 360, color: '#000' }}>{item.ten}</Text>
                                    </View>
                                    {item.loai === 2 &&
                                        <View style={{ flex: 0, marginLeft: 17 * STYLES.widthScreen / 360, justifyContent: 'center' }}>
                                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                <Entypo name="dot-single" color="#181B46" size={HEIGHT(20)} />
                                                <Text style={styles.subtitle}>Theo tuần :</Text>
                                                <Text style={styles.soluong}>7</Text>
                                            </View>
                                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                <Entypo name="dot-single" color="#F26322" size={HEIGHT(20)} />
                                                <Text style={styles.subtitle}>Theo tháng :</Text>
                                                <Text style={styles.soluong}>31</Text>
                                            </View>
                                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                <Entypo name="dot-single" color="#FEC804" size={HEIGHT(20)} />
                                                <Text style={styles.subtitle}>Theo năm :</Text>
                                                <Text style={styles.soluong}>365</Text>
                                            </View>
                                        </View>
                                    }
                                    {item.loai == 1 &&
                                        <Content padder>
                                            <Accordion
                                                dataArray={dataArray}
                                                animation={true}
                                                expanded={true}
                                                headerStyle={{ backgroundColor: "#fff" }}
                                                contentStyle={{ backgroundColor: "#fff" }}
                                            />
                                        </Content>
                                    }
                                    {item.loai == 3 && <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 17 * STYLES.widthScreen / 360 }}>
                                        <Entypo name="dot-single" color="#181B46" size={HEIGHT(20)} />
                                        <Text style={styles.subtitle}>Số lượng :</Text>
                                        <Text style={styles.soluong}>{(5 - index) * 13 + 12}</Text>
                                    </View>}
                                    {
                                        item.loai == 4 &&
                                        <TouchableOpacity>

                                        </TouchableOpacity>
                                    }
                                    {item.loai == 5 && <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 17 * STYLES.widthScreen / 360 }}>
                                        <Entypo name="dot-single" color="#181B46" size={HEIGHT(20)} />
                                        <Text style={styles.subtitle}>Điểm trung bình :</Text>
                                        <Text style={styles.soluong}>{(5 - index) * 13 + 12}</Text>
                                    </View>}
                                </View>
                            </TouchableOpacity>
                        ))}
                    </View>
                    <View style={{ height: HEIGHT(60) }}>

                    </View>
                </ScrollView>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 10 * STYLES.heightScreen / 640,
        // paddingTop: 5 * STYLES.heightScreen / 640,
        // flexDirection: 'row',
        alignItems: 'center'

    },
    leftCell: {
        padding: 8,
    },
    rightCell: {
        paddingVertical: HEIGHT(12),
        paddingRight: WIDTH(8),
        justifyContent: 'center',
        width: STYLES.widthScreen,
        //  borderColor: '#ddd',
        borderRadius: 10, borderWidth: 1,
        width: WIDTH(340),
        marginTop: HEIGHT(5),
        borderLeftWidth: WIDTH(4),
        borderLeftColor: '#765285',
        borderTopColor: '#ddd',
        borderBottomColor: '#ddd',
        borderRightColor: '#ddd'
    },
    image: {
        width: 45,
        height: 45,
        borderRadius: 5,
    },
    subtitle: {
        color: '#000',

        // marginTop: 5 * STYLES.widthScreen / 360,
    },
    soluong: {
        color: '#000',
        marginLeft: 5 * STYLES.widthScreen / 360,
        // marginTop: 5 * STYLES.widthScreen / 360,
    },
    tendv: {
        marginLeft: 25 * STYLES.widthScreen / 360,
        marginTop: 5 * STYLES.widthScreen / 360,
        // color:'#000'
    },
    box: {
        height: HEIGHT(100),
        width: WIDTH(200),
        borderRadius: 10,
        borderColor: '#eee',
        borderWidth: 1,
        marginLeft: WIDTH(10),
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
    }
});