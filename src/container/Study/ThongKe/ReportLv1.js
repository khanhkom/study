import React from 'react';
// import { Asset, AppLoading } from 'expo';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity
} from 'react-native';
import STYLES from '../../../config/styles.config';
const mariner = '#3B5F8F';
const mediumPurple = '#8266D4';
const tomato = '#F95B57';
const mySin = '#F3A646';
import HeaderReal from '../../../common/HeaderReal';

import { Card, ListItem, Button, Icon } from 'react-native-elements';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { HEIGHT, WIDTH } from '../../../config/Function';

export default class ReportLv1 extends React.Component<{}, AppState> {
    componentDidMount() {
        const sections = this.props.navigation.getParam('sections')
        console.log(sections)
    }
    render() {
        const sections = this.props.navigation.getParam('sections')
        const title = this.props.navigation.getParam('title')
        return (
            <View>
                <HeaderReal
                    onButton={() => this.props.navigation.goBack()}
                    title={title} />
                <View style={styles.body}>
                    {sections.map((item, index) => (
                        <TouchableOpacity 
                                    key={index}
                                    style={{
                                        flex:0,
                                        marginBottom:HEIGHT(5),
                                        paddingVertical:HEIGHT(5),
                                        backgroundColor:'white'
                                    }}
                                    onPress={() => { this.props.navigation.navigate('ReportLv2', { nhiemvu: item }) }}
                                    >
                                        <ListItem
                                            key={index}
                                            leftIcon={ <Ionicons size={WIDTH(20)} name={'logo-buffer'}/>}
                                            rightIcon={<Ionicons size={WIDTH(20)} name={'ios-arrow-forward'}/>}
                                            title={item.title}
                                        />
                                    </TouchableOpacity>
                    ))}

                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    body: {
        flex: 0,
        // paddingTop: 10 * STYLES.heightScreen / 640,
        height: STYLES.heightScreen,
        paddingTop: 5 * STYLES.heightScreen / 640,
        backgroundColor: '#eee'
    },
    item: {
        paddingBottom: 15 * STYLES.heightScreen / 640,
        paddingTop: 15 * STYLES.heightScreen / 640,
        borderBottomColor: '#ddd',
        borderTopColor: '#ddd',
        backgroundColor: '#fff',
        borderBottomWidth: 1,
        borderTopWidth: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 10 * STYLES.widthScreen / 360,
        paddingRight: 10 * STYLES.widthScreen / 360,
        justifyContent: 'space-between',
        marginTop: 5 * STYLES.heightScreen / 640
    },
    text: {
        fontSize: STYLES.fontSizeNormalText,
        color: '#000',
        width: STYLES.widthScreen * 0.8
    }
})