const StyleGuide = {
    typography: {
      title1: {
        fontSize: 44,
        lineHeight: 56,
        fontFamily: "Roboto-Bold",
      },
      title2: {
        fontSize: 32,
        lineHeight: 36,
        // fontFamily: "SFProDisplay-Light",
      },
      title3: {
        fontSize: 24,
        lineHeight: 28,
        // fontFamily: "SFProDisplay-Light",
      },
      large: {
        fontSize: 19,
        lineHeight: 24,
        // fontFamily: "SFProDisplay-Semibold",
      },
      regular: {
        fontSize: 17,
        lineHeight: 22,
        fontFamily: "Roboto-Light",
      },
      small: {
        fontSize: 14,
        lineHeight: 18,
        fontFamily: "Roboto-Light",
      },
      micro: {
        fontSize: 8,
        lineHeight: 8,
        fontFamily: "Roboto-Bold",
      },
    },
    spacing: {
      tiny: 8,
      small: 16,
      base: 24,
      large: 48,
      xLarge: 64,
    },
  };
  
  export default StyleGuide;