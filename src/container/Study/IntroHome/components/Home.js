import * as React from "react";
import {
  View, Text, StyleSheet, ScrollView, SafeAreaView, InteractionManager,
} from "react-native";

import Category from "./Category";
import CityCard from "./CityCard";
import HomeCard, { Home } from "./HomeCard";
import StyleGuide from "./StyleGuide";
const homes: [Home, Home] = [
  {
    category1: "Entire apartment",
    category2: "1 bedroom",
    title: "Centric studio with roof top terrace",
    price: {
      amount: 85,
      currency: "CHF",
    },
    // eslint-disable-next-line max-len
    picture: "http://www.worldtrans.vn/upload/images/khamphadulich/10-thanh-pho-dep-nhat-ve-dem-cua-vn-1.png",
  },
  {
    category1: "Entire apartment",
    category2: "1 bedroom",
    title: "Great studio in Zurich center",
    price: {
      amount: 52,
      currency: "CHF",
    },
    // eslint-disable-next-line max-len
    picture: "https://znews-photo.zadn.vn/w860/Uploaded/jaroin/2015_09_14/9.jpg",
  },
];

interface ExploreProps {
  onLoad: () => void;
}

// eslint-disable-next-line react/prefer-stateless-function
export default class Explore extends React.PureComponent<ExploreProps> {
  explore = React.createRef();

  city = React.createRef();

  cities = React.createRef();

  componentDidMount() {
    const { onLoad } = this.props;
    setTimeout(onLoad, 1000);
  }

  render() {
    return (
      <ScrollView>
        <SafeAreaView>
          <View>
            <Text style={styles.title1} ref={this.explore}>Explore</Text>
            <ScrollView horizontal style={styles.scrollView} contentContainerStyle={styles.container}>
              <Category label="Homes" image="http://www.dulichvietnam.com.vn/data/Maceio.png" />
              <Category label="Experiences" image="http://www.oxygenjunglevillas.com/wp-content/uploads/2017/11/waterfal.jpg" />
              <Category label="Restaurants" image="http://image.sggp.org.vn/w570/Uploaded/2019/duaeymdrei/2018_01_21/u8d_uant.jpg" />
            </ScrollView>
            <View>
              <Text style={styles.title2} ref={this.city}>Zürich</Text>
              <ScrollView
                horizontal
                style={styles.scrollView}
                contentContainerStyle={styles.container}
              >
                <HomeCard home={homes[0]} />
                <HomeCard home={homes[1]} />
              </ScrollView>
            </View>
            <View ref={this.cities}>
              <ScrollView horizontal style={styles.scrollView} contentContainerStyle={styles.container}>
                <CityCard label="Cape Town" image="https://deviet.vn/wp-content/uploads/2017/03/top-10-thanh-pho-dep-nhat-chau-au6.jpg" />
                <CityCard label="London" image="http://streaming1.danviet.vn/upload/2-2018/images/2018-04-22/13-1524365344-width650height433.jpg" />
                <CityCard label="Los Angeles" image="http://review.siu.edu.vn/Upload/Siu16/5%20thanh%20pho%20dep%20nhat%20the%20gioi%201.jpg" />
                <CityCard label="Miami" image="https://www.chudu24.com/wp-content/uploads/2017/03/4-43.jpg" />
                <CityCard label="Nairobi" image="http://inspired.dkn.tv/wp-content/uploads/2016/12/anh209.jpg" />
                <CityCard label="Paris" image="https://www.baogialai.com.vn/dataimages/201710/original/images2581935_a_2.jpg" />
                <CityCard label="San Francisco" image="https://evaair.biz.vn/Img.ashx?635466213449210000.jpg" />
                <CityCard label="Tokyo" image="https://stvgo.vn/wp-content/uploads/2019/03/STV-L%C3%A2u-%C4%91%C3%A0i-Osaka.jpg" />
              </ScrollView>
            </View>
          </View>
        </SafeAreaView>
      </ScrollView>
    );
  }
}


const styles = StyleSheet.create({
  title1: {
    ...StyleGuide.typography.title1,
    paddingLeft: StyleGuide.spacing.base,
  },
  title2: {
    ...StyleGuide.typography.title2,
    paddingLeft: StyleGuide.spacing.base,
  },
  scrollView: {
    paddingHorizontal: StyleGuide.spacing.base,
    marginBottom: StyleGuide.spacing.base,
  },
  container: {
    paddingRight: StyleGuide.spacing.base,
  },
});
