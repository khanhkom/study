const Class = function (mixins) {
    const proto = {};
    for (let i = 0, l = arguments.length; i < l; i++) {
      let mixin = arguments[i];
      if (typeof mixin === "function") mixin = mixin.prototype;
      for (const key in mixin) proto[key] = mixin[key];
    }
    if (!proto.initialize) proto.initialize = function () {};
    proto.constructor = function (a, b, c, d, e, f, g, h) {
      return new proto.initialize(a, b, c, d, e, f, g, h);
    };
    proto.constructor.prototype = proto.initialize.prototype = proto;
    return proto.constructor;
  };
  
  // Utility command factories
  
  const point = function (c) {
    return function (x, y) {
      return this.push(c, x, y);
    };
  };
  
  const arc = function (c, cc) {
    return function (x, y, rx, ry, outer) {
      return this.push(c, Math.abs(rx || x), Math.abs(ry || rx || y), 0, outer ? 1 : 0, cc, x, y);
    };
  };
  
  const curve = function (t, s, q, c) {
    return function (c1x, c1y, c2x, c2y, ex, ey) {
      const l = arguments.length; const
        k = l < 4 ? t : l < 6 ? q : c;
      return this.push(k, c1x, c1y, c2x, c2y, ex, ey);
    };
  };
  
  // SVG Path Class
  
  var SVGPath = Class({
  
    initialize(path) {
      if (path instanceof SVGPath) {
        this.path = [Array.prototype.join.call(path.path, " ")];
      } else if (path && path.applyToPath) { path.applyToPath(this); } else { this.path = [path || "m0 0"]; }
    },
  
    push() {
      this.path.push(Array.prototype.join.call(arguments, " "));
      return this;
    },
  
    reset() {
      this.path = [];
      return this;
    },
  
    move: point("m"),
    moveTo: point("M"),
  
    line: point("l"),
    lineTo: point("L"),
  
    curve: curve("t", "s", "q", "c"),
    curveTo: curve("T", "S", "Q", "C"),
  
    arc: arc("a", 1),
    arcTo: arc("A", 1),
  
    counterArc: arc("a", 0),
    counterArcTo: arc("A", 0),
  
    close() {
      return this.push("z");
    },
  
    toSVG() {
      return this.path.join(" ");
    },
  
  });
  
  SVGPath.prototype.toString = SVGPath.prototype.toSVG;
  
  module.exports = SVGPath;