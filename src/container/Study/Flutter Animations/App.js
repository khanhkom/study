// @flow
import React from 'react';
import { Sections, type Section } from './index';

const mariner = '#3B5F8F';
const mediumPurple = '#8266D4';
const tomato = '#F95B57';
const mySin = '#F3A646';

const sections: Section[] = [
  {
    title: 'SUNGLASSES',
    leftColor: mediumPurple,
    rightColor: mariner,
    image: 'https://i.ytimg.com/vi/hdyMGH3dnXA/maxresdefault.jpg',
  },
  {
    title: 'FURNITURE',
    leftColor: tomato,
    rightColor: mediumPurple,
    image: 'https://i.ytimg.com/vi/hdyMGH3dnXA/maxresdefault.jpg',
  },
  {
    title: 'JEWELRY',
    leftColor: mySin,
    rightColor: tomato,
    image: 'https://i.ytimg.com/vi/hdyMGH3dnXA/maxresdefault.jpg',
  },
  {
    title: 'HEADWEAR',
    leftColor: 'white',
    rightColor: tomato,
    image: 'https://i.ytimg.com/vi/hdyMGH3dnXA/maxresdefault.jpg',
  },
];

export default class App extends React.Component<{}, AppState> {
  state = {
    ready: false,
  };

  async componentDidMount() {
    // await Promise.all(sections.map(section => Asset.loadAsync(section.image)));
    // this.setState({ ready: true });
  }

  render() {
    return (
      <Sections {...{ sections }} />
    );
  }
}