// @flow
import React from 'react';
import { StatusBar, View, ActivityIndicator, StyleSheet } from 'react-native';
// Two implementations of the story components.
// One using linear interpolation which doesn't make it a perfect cube and one with setNativeProps
import { Stories, Stories2,Stories3 } from './Index';

const stories = [
  {
    id: '2',
    source: 'https://hinhchuctet.com/wp-content/uploads/2018/09/hinh-gai-xinh-de-thuong-nhat-2.jpg',
    user: 'derek.russel',
    avatar: 'https://img2.thuthuatphanmem.vn/uploads/2019/01/04/anh-dep-gai-xinh_025059121.png',
  },
  {
    id: '4',
    source:'http://f1.docbao.vn/images/site-1/20170718/web/thien-than-me-doc-sach-xinh-dep-khien-nguoi-khac-kho-roi-mat-10-140747.jpg',
    user: 'jmitch',
    avatar: 'https://img2.thuthuatphanmem.vn/uploads/2019/01/04/anh-dep-gai-xinh_025059121.png',
  },
  {
    id: '5',
    source:'http://f1.docbao.vn/images/site-1/20170718/web/thien-than-me-doc-sach-xinh-dep-khien-nguoi-khac-kho-roi-mat-10-140747.jpg',
    user: 'monicaa',
    avatar: 'https://img2.thuthuatphanmem.vn/uploads/2019/01/04/anh-dep-gai-xinh_025059121.png',
  },
  {
    id: '3',
    source: 'http://f1.docbao.vn/images/site-1/20170718/web/thien-than-me-doc-sach-xinh-dep-khien-nguoi-khac-kho-roi-mat-10-140747.jpg',
    user: 'alexandergarcia',
    avatar: 'https://img2.thuthuatphanmem.vn/uploads/2019/01/04/anh-dep-gai-xinh_025059121.png',
  },
  {
    id: '1',
    source: 'http://f1.docbao.vn/images/site-1/20170718/web/thien-than-me-doc-sach-xinh-dep-khien-nguoi-khac-kho-roi-mat-10-140747.jpg',
    user: 'andrea.schmidt',
    avatar: 'https://img2.thuthuatphanmem.vn/uploads/2019/01/04/anh-dep-gai-xinh_025059121.png',
  },
];

type AppState = {
  ready: boolean,
};

export default class App extends React.Component<{}, AppState> {
  state = {
    ready: false,
  };

//   async componentDidMount() {
//     await Promise.all(stories.map(story => Promise.all([
//       Asset.loadAsync(story.source),
//       Asset.loadAsync(story.avatar),
//     ])));
//     this.setState({ ready: true });
//   }

  render() {
    // const { ready } = this.state;
    // if (!ready) {
    //   return (
    //     <View style={styles.container}>
    //       <ActivityIndicator size="large" color="white" />
    //     </View>
    //   );
    // }
    return (
      <View style={{ flex: 1 }}>
        <StatusBar barStyle="light-content" />
        <Stories3 {...{ stories }} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#222222',
  },
});