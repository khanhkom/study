import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    Image,
    Animated
} from 'react-native';
HEADER_MAX_HEIGHT = 120;
HEADER_MIN_HEIGHT = 70;
PROFILE_IMAGE_MAX_HEIGHT = 80;
PROFILE_IMAGE_MIN_HEIGHT = 60;
class Test extends Component {
    constructor(props) {
        super(props);
        this.state = {
            scrollY: new Animated.Value(0)
        }
    }
    render() {
        const headerHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
            outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
            extrapolate: 'clamp'
        })
        const profileImageHeight = this.state.scrollY.interpolate({
            inputRange: [0, PROFILE_IMAGE_MAX_HEIGHT - PROFILE_IMAGE_MIN_HEIGHT],
            outputRange: [PROFILE_IMAGE_MAX_HEIGHT, PROFILE_IMAGE_MIN_HEIGHT],
            extrapolate: 'clamp'
        })
        const profileImageMarginTop = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
            outputRange: [HEADER_MAX_HEIGHT - (PROFILE_IMAGE_MAX_HEIGHT), 0],
            extrapolate: 'clamp'
        })
        const headerZindex = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT],
            outputRange: [0, 1],
            extrapolate: 'clamp'
        })
        const fontSizeName = this.state.scrollY.interpolate({
            inputRange: [0, 1
            ],
            outputRange: [26, 20],
            extrapolate: 'clamp'
        })
        const fontSizeSdt = this.state.scrollY.interpolate({
            inputRange: [0, 5,10,15
            ],
            outputRange: [20, 12,10,0],
            extrapolate: 'clamp'
        })
        return (
            <View style={{ flex: 1 }}>
                <Animated.View style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    backgroundColor: 'lightskyblue',
                    height: headerHeight,
                    zIndex: headerZindex,
                    flexDirection: 'row',
                }}>
                    <Animated.View style={{
                        height: profileImageHeight,
                        width: profileImageHeight,
                        borderRadius: PROFILE_IMAGE_MAX_HEIGHT / 2,
                        backgroundColor: 'white',
                        borderWidth: 3,
                        borderColor: 'white',
                        overflow: 'hidden',
                        marginTop: profileImageMarginTop,
                        marginLeft: 10
                    }}>
                        <Image source={{ uri: 'https://hinhchuctet.com/wp-content/uploads/2018/09/hinh-gai-xinh-de-thuong-nhat-2.jpg' }}
                            style={{
                                flex: 1, width: null, height: null
                            }}
                        />
                    </Animated.View>
                    <Animated.View style={{
                        height: profileImageHeight,
                        backgroundColor: 'white',
                        borderWidth: 3,
                        borderColor: 'white',
                        overflow: 'hidden',
                        marginTop: profileImageMarginTop,
                        marginLeft: 10
                    }}>
                    <View>
                        <Animated.View >
                            <Animated.Text style={{fontWeight: 'bold', fontSize: fontSizeName,
                            }}>Khanh Nguyen</Animated.Text>
                        </Animated.View>
                        <Animated.View >
                            <Animated.Text style={{ fontSize: fontSizeSdt, fontWeight: 'bold' }}>Nguyen Huu Khanh</Animated.Text>
                        </Animated.View>
                    </View>
                    </Animated.View>
                </Animated.View>
                <ScrollView style={{ flex: 1 }}
                    scrollEventThrottle={16}
                    onScroll={
                        Animated.event([
                            { nativeEvent: { contentOffset: { y: this.state.scrollY } } }
                        ])
                    }
                >
                    <View style={{ height: 1000 }}></View>

                </ScrollView>
            </View>
        )
    }
}
export default Test