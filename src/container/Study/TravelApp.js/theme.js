const colors = {
    black: '#000',
    white: '#FFF',
    gray: '#DCE0E9',
    caption: '#BCCCD4',
    active: '#007BFA',
  };
  import {WIDTH,HEIGHT} from '../../../config/Function'
  const sizes = {
    base:HEIGHT(16),
    font: 14,
    padding: HEIGHT(36),
    margin:HEIGHT(36),
    title: 24,
    radius: WIDTH(12),
  };
  
  export {
    colors,
    sizes,
  };