import React from 'react';
import { createStackNavigator } from 'react-navigation';

import List from './List';
import Article from './Article';

export default createStackNavigator(
  {
    List,
    Article
  },
  {
    initialRouteName: "List",
  }
);