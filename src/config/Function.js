import { Dimensions } from "react-native";
const { width, height } = Dimensions.get('window');
import colors from './colors';
import { FetchJson, GetEventsByTime } from '../servers/serivce.config'
import {
	DATA1, DATA2, DATA3, DATA4, DATA5, DATA6, DATA7, DATA8,
	DATA9, DATA10, DATA11, DATA12, DATA_FORMAT_MONTH, ERROR, DATA_FAKE_SERVER
} from "./default";

export const responsiveHeight = (h) => {
	return height * (h / 100);
};

export const responsiveWidth = (w) => {
	return width * (w / 100);
};
export const WIDTH = (w) => {
	return width * (w / 360);
};
export const HEIGHT = (h) => {
	return height * (h / 640);
};

export const calTime = (timeStart,timeEnd) => {
	let minusStart=timeStart.getTime();
	let minusEnd=timeEnd.getTime();
	let res=(minusEnd-minusStart)/1000;
	return Math.floor(res/60);
};
export const outNumberSmall = (xy) => {
	if (xy < 10) return "0" + xy.toString();
	else return xy.toString();
};
export const checkUser = (username) => {
	console.log("====>>>>username", username);
	if (username == "" || username == undefined) return ERROR;
	let str = username.split(';');
	return str[str.length - 1];
};
export const responsiveWidthComponent = (widthComponent, w) => {
	return widthComponent * (w / 100);
};

export const responsiveFontSize = (f) => {
	return Math.sqrt((height * height) + (width * width)) * (f / 100);
};

export const responsiveVar = (h, v) => {
	return v * (h / 100);
};

export const slitNumber = (str) => {
	let res = "";
	for (let i = 0; i < str.length; i++)
		if (str[i] != '.') res = res + " " + str[i];
	return res;
};

export const calListViewMonth = (day, month, year) => {
	//dua ra list cac ngay trong 1 thang theo dung view lich 
	//va ngay hien tai dang la ngay nao
	let DataDay = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
	let dateFirstMonth = new Date(year, month - 1, 1);
	let maxN = (year % 4 == 0 && month == 2) ? 29 : DataDay[month];
	let str = [];
	let thu = dateFirstMonth.getDay() == 0 ? 6 : dateFirstMonth.getDay() - 1;
	for (let u = 1; u <= thu; u++) str.push('');
	for (let i = 1; i <= maxN; i++) str.push(i);
	return {
		arr: str,
		selected: thu - 1 + day
	}
};
export const calMixColor = (colorA, colorB) => {
	if (
		(colorA == colors.blueCalendar && colorB == colors.greenCalendar)
		|| (colorB == colors.blueCalendar && colorA == colors.greenCalendar)
	) return colors.mixBlueGreen;
	if (
		(colorA == colors.blueCalendar && colorB == colors.yellowCalendar)
		|| (colorB == colors.blueCalendar && colorA == colors.yellowCalendar)
	) return colors.mixBlueYellow;
	if (
		(colorA == colors.yellowCalendar && colorB == colors.greenCalendar)
		|| (colorB == colors.yellowCalendar && colorA == colors.greenCalendar)
	) return colors.mixGreenYellow;
	return colors.mixAll;
};

export const calEventByDay = (dataEvent, day, month, year) => {
	///tinh cac mang event cho cac hoat dong
	let data = sortForEvent(dataEvent);
	let arr = [];
	//Tinh su kien xay ra trong 1 ngay
	for (let u = 0; u <= 31; u++) {
		arr.push({
			day: u,
			dataEvent: [],
			dataColorEvent: [],
			minDay: 50,
			maxDay: 0,
		})
	}
	for (let i = 0; i < data.length; i++) {
		for (let j = data[i].dayStart; j <= data[i].dayEnd; j++) {
			arr[j].dataEvent.push(data[i]);
			let ok = 1;
			arr[j].minDay = Math.min(arr[j].minDay, data[i].dayStart);
			arr[j].maxDay = Math.max(arr[j].maxDay, data[i].dayEnd);

			if (j == data[i].dayStart) ok = 0;
			if (j == data[i].dayEnd) ok = 2;
			if (data[i].dayStart == data[i].dayEnd) {

				arr[j].dataColorEvent.push({
					idEvent: i,
					idFormat: 0,
				})

				arr[j].dataColorEvent.push({
					idEvent: i,
					idFormat: 2,
				})
			}
			else
				arr[j].dataColorEvent.push({
					idEvent: i,
					idFormat: ok,
				})
		}
	}

	return arr;
};

export const resultIndexDay = (day, month, year) => {
	let dateFirstMonth = new Date(year, month - 1, 1);
	let thu = dateFirstMonth.getDay() == 0 ? 6 : dateFirstMonth.getDay() - 1;
	return thu - 1 + day

};
export const calDayStandard = (time, dayPlus) => {
	if (DATA_FORMAT_MONTH[time.month] >= dayPlus) {
		return time.year + "-" + time.month + "-" + dayPlus + " " + time.time;
	} else {
		let month = time.month;
		let day = dayPlus;
		for (let i = time.month; i < DATA_FORMAT_MONTH.length; i++) {
			if (DATA_FORMAT_MONTH[i] >= day) {
				return time.year + "-" + i.toString() + "-" + day + " " + time.time;
			} else day = day - DATA_FORMAT_MONTH[i];
		}
	}
	return time.year + "-12-12 23:59";
};
export const calDayForRepeat = (timeBD, timeKT, dayPlus) => {
	return {
		timeStart: calDayStandard(timeBD, timeBD.day + dayPlus),
		timeEnd: calDayStandard(timeKT, timeKT.day + dayPlus)
	}
};
export const mapToStanđardFormat = (day) => {
	let res = day;
	if (parseInt(day) >= 0 && parseInt(day) <= 9) res = "0" + day;
	return res;
};
export const mapNumberPhone = (res) => {
	// Số điện thoại phòng giáo vụ là: #0240.123.456# hoặc #0456.789.123#
	let tmg = res;
	let str = tmg.split("#");
	let speak = "";
	tmg = "";
	for (let i = 0; i < str.length; i++) tmg = tmg + str[i];
	if (str.length > 3) speak = speak + str[0] + slitNumber(str[1]) + str[2] + slitNumber(str[3]);
	else speak = speak + str[0] + slitNumber(str[1]);
	return {
		result: tmg,
		speak: speak
	}
};
export const cmpDay = (one, two) => {
	let timeOne = mapToYearMonthDay(one);
	let timeTwo = mapToYearMonthDay(two);
	if (timeOne.year > timeTwo.year) return true;
	else if (timeOne.year === timeTwo.year) {
		if (timeOne.month > timeTwo.month) return true;
		else if (timeOne.month == timeTwo.month) {
			if (timeOne.day > timeTwo.day) return true;
			else if (timeOne.day == timeTwo.day) {
				if (timeOne.hour > timeTwo.hour) return true;
				else if (timeOne.hour == timeTwo.hour)
					if (timeOne.minute > timeTwo.minute) return true;
			}
		}
	}
	return false;
}
export const cmpEvent = (one, two) => {
	let timeOne = one;
	let timeTwo = two;
	if (timeOne.yearStart > timeTwo.yearStart) return true;
	else if (timeOne.yearStart === timeTwo.yearStart) {
		if (timeOne.monthStart > timeTwo.monthStart) return true;
		else if (timeOne.monthStart == timeTwo.monthStart) {
			if (timeOne.dayStart > timeTwo.dayStart) return true;
			else if (timeOne.dayStart == timeTwo.dayStart) {
				return true;
			}
		}
	}
	return false;
}
export const sortForDay = (arr) => {
	let a = arr;
	for (let i = 0; i < a.length - 1; i++)
		for (let j = i + 1; j < a.length; j++)
			if (cmpDay(a[j].time, a[i].time)) {
				let tg = a[i];
				a[i] = a[j];
				a[j] = tg;
			}
	return a;
};
export const sortForEvent = (arr) => {
	let a = arr;
	for (let i = 0; i < a.length - 1; i++)
		for (let j = i + 1; j < a.length; j++)
			if (cmpEvent(a[j], a[i])) {
				let tg = a[i];
				a[i] = a[j];
				a[j] = tg;
			}
	return a;
};

export const handspace = (res) => {
	let tmg = res;
	let str = tmg.split("@@");
	let speak = "";
	for (let i = 0; i < str.length; i++) speak = speak + str[i];
	return {
		result: tmg,
		speak: speak
	}
};
export const calArrMail = (arr) => {
	if (arr == undefined || arr == null) return [];
	let dateCurrent = new Date();
	let res = [];
	for (let i = 0; i < arr.length; i++) {
		let timeTmp = arr[i].timereminder;
		if (arr[i].timereminder==null || arr[i].timereminder==undefined) timeTmp=arr[i].time;
		if (timeTmp != '' && timeTmp!=null) {
			let timeRemind = mapToYearMonthDay(timeTmp);
			if (timeRemind.year == dateCurrent.getFullYear())
				if (timeRemind.month < (dateCurrent.getMonth() + 1)) res.push(arr[i])
				else if (timeRemind.month == (dateCurrent.getMonth() + 1))
					if (timeRemind.day < dateCurrent.getDate()) res.push(arr[i]);
					else if (timeRemind.day == dateCurrent.getDate())
						if (timeRemind.hour < dateCurrent.getHours()) res.push(arr[i]);
						else if (timeRemind.hour == dateCurrent.getHours())
							if (timeRemind.minute <= dateCurrent.getMinutes()) res.push(arr[i]);
		}
	}
	return sortForDay(res);
};
export const countMainNotSeen = (arr) => {
	let res = 0;
	if (arr == undefined || arr == null) return res;
	for (let i = 0; i < arr.length; i++)
		if (arr[i].seen == false) res++;
	return res;
};

export const handhttp = (res) => {
	let tmg = res;
	let str = tmg.split(" ");
	let speak = "";
	for (let i = 0; i < str.length; i++) {
		if (str[i].indexOf('http') < 0) speak = speak + str[i] + " ";
	}
	return {
		result: tmg,
		speak: speak
	}
};
export const mapToYearMonthDay = (str) => {
	//str=2019-01-04 02:01

	let arrOne = str.split(" ");
	let arrTwo = arrOne[0].split('-');
	let arrThree = arrOne[1].split(':');
	return {
		year: parseInt(arrTwo[0]),
		month: parseInt(arrTwo[1]),
		day: parseInt(arrTwo[2]),

		hour: parseInt(arrThree[0]),
		minute: parseInt(arrThree[1]),

		time: arrOne[1],
	}
};
export const isSmallerTen= (str)=>{
	if (parseInt(str) < 10) return "0"+parseInt(str).toString();
	return str;
}
export const formatDateForServer = (str) => {
	//str=2019-01-04 02:01
	if (str=="") return str;
	let arrOne = str.split(" ");
	let arrTwo = arrOne[0].split('-');
	let arrThree = arrOne[1].split(':');
	let res = isSmallerTen(arrTwo[0]) + isSmallerTen(arrTwo[1]) + isSmallerTen(arrTwo[2]) + isSmallerTen(arrThree[0]) + isSmallerTen(arrThree[1]);
	return res;
};
export const calTimeHour = (str) => {
	if (str=="") return str;
	let arr = str.split(':');
	return arr[0]+':'+arr[1];
};
export const checkFormat = (str) => {
	if (str==undefined || str==null) return '[]';
	else return str;
};
export const loadDataByMonth = async (self,arr, month, year, account) => {
	//load data tu server theo thang
	let res = [];
	if (arr != undefined && arr != null) for (let i = 0; i < arr.length; i++) {
		if (arr[i].monthStart == month) res.push(arr[i]);
	}
	// let monthString=month < 10 ? "0"+month.toString() : month;
	// let json = {
	// 	userID: account.IdDangVien,
	// 	time: year.toString() + "-" + monthString,
	// 	size: 5,
	// 	pageindex: 1,
	// }
	// await self.onChangeLoading(true);
	// await FetchJson(GetEventsByTime, json)
	// 	.then((response) => {
	// 		self.onChangeLoading(false);
	// 		let dataLoadFromServer = JSON.parse(response.d).rows;	
	// 		dataLoadFromServer.map((value, index) => {
	// 			 if (value.timestart!=null &&value.timeend!=null){
	// 				let timeBD = mapToYearMonthDay(value.timestart);
	// 				let timeKT = mapToYearMonthDay(value.timeend);
	// 				let event = {
	// 					idmessage: value.idmessage,
	// 					sender: value.sender,
	// 					title: value.title,
	// 					timeStart: timeBD.time,
	// 					dayStart: timeBD.day,
	// 					monthStart: timeBD.month,
	// 					yearStart: timeBD.year,
	// 					timeEnd: timeKT.time,
	// 					dayEnd: timeKT.day,
	// 					monthEnd: timeKT.month,
	// 					yearEnd: timeKT.year,
	// 					location: value.location,
	// 					result:value.location,
	// 					time: value.time,
	// 					reminder: value.reminder,
	// 					timereminder: value.timereminder,
	// 					repeat: value.repeat,
	// 					repeatnumber: value.repeatnumber,
	// 					timerepeat: value.timerepeat,
	// 					seen: value.seen,
	// 					seennote: value.seennote,
	// 					details: value.details,
	// 					priority: value.priority,
	// 					newsid: value.newsid,
	// 				};
	// 				res.push(event);
	// 			}
	// 		})
	// 	})
	return res;
};
