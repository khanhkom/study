
export function incCount() {
    return { type: 'INC_COUNT'};
}

export function decCount() {
    return { type: 'DEC_COUNT'};
}